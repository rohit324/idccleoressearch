/**
 * 
 */
package research.threadpool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ragarwal
 *
 */
public class PersistantEventExecutorSample {
	
	static SynchronousQueue<Runnable> synchronousQueue = new SynchronousQueue<Runnable>(true);
//	public static ExecutorService workerPool = new ThreadPoolExecutor(0, 250,
//            60L, TimeUnit.SECONDS,
//            synchronousQueue);
	
	public static ExecutorService workerPool = Executors.newFixedThreadPool(200);
	public static AtomicInteger counter = new AtomicInteger(0);
	public static int totalTask = 1280000;
	private static File storeDir = new File("c:\\temp1");
	private static String storage = "store.data";
	private static String completed = "completed.data";

	static {
		new File(storeDir,storage).delete();
		new File(storeDir,completed).delete();
	}

	public static void main(String[] args) throws Exception {
		long startTime = System.currentTimeMillis();
		PersistantEventExecutorSample.workerPool.execute(new WorkerTask(new Message("test message")));
		for(int i=1;i<totalTask;i++) {
			Message message = new Message(""+i);
			PersistantEventExecutorSample.persistMessage(message);
			workerPool.submit(new PreprocessExecutorTask(message));
		}
		workerPool.awaitTermination(100, TimeUnit.SECONDS);
		System.out.println("total messages processed = " + counter.get());
		stream.flush();
		stream.close();
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken for processing " + (endTime-startTime)/100);
	}
	
	static File file = new File(storeDir,storage);
	static FileOutputStream stream;

	static File doneFile = new File(storeDir,completed);
	static FileOutputStream doneStream;

	
	static {
		try {
			stream = new FileOutputStream(file,true);
			doneStream = new FileOutputStream(doneFile,true);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static synchronized void persistMessage(Message message) throws Exception {
		stream.write(message.getInstructions().getBytes());
		stream.write("\n".getBytes());
	}
	
	public static synchronized void persistDoneMessages(Message message) throws Exception {
		doneStream.write(message.getInstructions().getBytes());
		doneStream.write("\n".getBytes());
	}


}

class WorkerTask implements Runnable {
	
	private Message message;
	
	
		public WorkerTask(Message message) {
		this.message = message;
	}

	public void run() {
		ComplexMessageProcessing.processMessage(message);
		int incrementAndGet = PersistantEventExecutorSample.counter.incrementAndGet();
		try {
			PersistantEventExecutorSample.persistDoneMessages(message);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("Processing " + incrementAndGet);
		if(incrementAndGet==PersistantEventExecutorSample.totalTask) {
			PersistantEventExecutorSample.workerPool.shutdown();
		}
	}
}


class ComplexMessageProcessing {
	
	public static void processMessage(Message message) {
		for(int i=0;i<1000000;i++) {
			for(int j=0;j<10000;j++) {
				float comlexCal = 676764^676^676*898/67*4^7^7*89;
				comlexCal = 676764^676^676*898/67*4^7^7*89;
				for(int k=0;k<1000;k++) {
					comlexCal = 676764^676^676*898/67*4^7^7*89;
					comlexCal = 676764^676^676*898/67*4^7^7*89;
				}
			}
		}
	}
}


class Message {
	
	private String instructions;

	public Message(String instructions) {
		super();
		this.instructions = instructions;
	}

	public String getInstructions() {
		return instructions;
	}
	
}


class PreprocessExecutorTask implements Runnable {
	
	private Message message;
	
	
	
	public PreprocessExecutorTask(Message message) {
		this.message = message;
	}

	public void run() {
//		try {
//			PersistantEventExecutorSample.persistMessage(message);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		PersistantEventExecutorSample.workerPool.execute(new WorkerTask(message));
	}

	
}


