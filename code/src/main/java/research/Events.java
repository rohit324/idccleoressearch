package research;

import com.cleo.lexicom.XMLBeaner;
import com.cleo.util.logger.XMLLogElement;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import simplepersistance.ExecutionUtil;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sbelur on 21/10/15.
 */
public class Events {

	public static String getElement(XMLLogElement event) throws Exception {

		if (event == null)
			return null;
		XMLLogElement element = ((XMLLogElement) event);
		StringBuilder eventBuffer = new StringBuilder();
		for (int i = 0; i < element.getLength(); i++) {
			eventBuffer.append(element.getLocalName(i) + "=="
					+ element.getValue(i) + "\n");
		}

		return eventBuffer.toString();
	}

	public static XMLLogElement generateEvent() {
		XMLLogElement xmlLogElement = null;
		try {
			xmlLogElement = new XMLLogElement("file", "dateTime",
					new String[] {}, new String[] {}, "", "yellow");
			xmlLogElement.attribute("test", "testing");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return xmlLogElement;
	}
}
