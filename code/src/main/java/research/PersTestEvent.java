package research;

import java.io.Serializable;

/**
 * Created by sbelur on 21/10/15.
 */
public class PersTestEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    private  Long deliveryId;
    private  String deliveryIdStr;
    private final String evt;
    private  long start;
    private final long startInMs;
    private long timeSentToWorker;


    public String getDeliveryIdStr() {
        return deliveryIdStr;
    }

    public void setDeliveryIdStr(String deliveryIdStr) {
        this.deliveryIdStr = deliveryIdStr;
    }

    public PersTestEvent(String evt,long start,long startInMs) {
        this.evt = evt;
        this.start = start;
        this.startInMs = startInMs;
    }

    public long getStartInMs() {
        return startInMs;
    }

    public void setDeliveryId(Long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public Long getDeliveryId() {
        return deliveryId;
    }

    public String getEvt() {
        return evt;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {

        this.start = start;
    }

    public long getTimeSentToWorker() {
        return timeSentToWorker;
    }

    public void setTimeSentToWorker(long timeSentToWorker) {
        this.timeSentToWorker = timeSentToWorker;
    }
}
