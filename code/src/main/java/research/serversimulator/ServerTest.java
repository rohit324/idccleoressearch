package research.serversimulator;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerTest implements Runnable {
	
	protected int          serverPort   = 9051;
	protected ServerSocket serverSocket = null;
	protected boolean      isStopped    = false;
	protected ExecutorService threadPool = Executors.newFixedThreadPool(150);

	public ServerTest(int port){
		this.serverPort = port;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ServerTest server = new ServerTest(9052);
		new Thread(server).start();

	}
	
	@Override
	public void run(){
		openServerSocket();
		while(!isStopped()){
			Socket clientSocket = null;
			try {
				clientSocket = this.serverSocket.accept();
			} catch (IOException e) {
				if(isStopped()) {
					System.out.println("Server Stopped.") ;
					break;
				}
				throw new RuntimeException(
						"Error accepting client connection", e);
			}
			threadPool.submit(
					new Worker(clientSocket,
							"Thread Pooled Server"));
		}
		threadPool.shutdown();
		System.out.println("Server Stopped.") ;
	}


	private synchronized boolean isStopped() {
		return this.isStopped;
	}

	public synchronized void stop(){
		this.isStopped = true;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			throw new RuntimeException("Error closing server", e);
		}
	}

	private void openServerSocket() {
		try {
			this.serverSocket = new ServerSocket(this.serverPort);
			this.serverSocket.setReuseAddress(true);
		} catch (IOException e) {
			throw new RuntimeException("Cannot open port", e);
		}
	}
	
	static Random random = new Random(10);


}

class Worker implements Runnable{

	protected Socket clientSocket = null;
	protected String serverText   = null;

	public Worker(Socket clientSocket, String serverText) {
		this.clientSocket = clientSocket;
		this.serverText   = serverText;
	}


	public void run() {
		try {
			InputStream input  = clientSocket.getInputStream();
//			int clientData = input.read();
//			input.close();
			int waitTime = ServerTest.random.nextInt(10);
			OutputStream output = clientSocket.getOutputStream();
//			Thread.sleep(new Double(random.nextDouble()).intValue());
			Thread.sleep(waitTime);
			output.write((""+waitTime).getBytes());
			output.close();
//			input.close();
//			System.out.println("Request processed: " + waitTime);
			clientSocket.close();
		} catch (Exception e) {
			//report exception somewhere.
			e.printStackTrace();
		}
	}
}
