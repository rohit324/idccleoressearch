package research.triggers;



public class FileTrigger extends Trigger {
	
	protected String sourceFile;
	protected Long fileSize;
	protected Long fileTimestamp;

	
	public String getSourceFile() {
		return sourceFile;
	}
	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public Long getFileTimestamp() {
		return fileTimestamp;
	}
	public void setFileTimestamp(Long fileTimestamp) {
		this.fileTimestamp = fileTimestamp;
	}

}
