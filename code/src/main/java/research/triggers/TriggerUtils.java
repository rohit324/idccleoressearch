/**
 * 
 */
package research.triggers;

/**
 * @author ragarwal
 *
 */
public class TriggerUtils {
	
	public static FileCreatedTrigger createSampleTrigger() {
		FileCreatedTrigger trigger = new FileCreatedTrigger();
		trigger = new FileCreatedTrigger();
		trigger.setCommand("STOR");
		trigger.setSessionId("sessionId");
		trigger.setStatus(Trigger.Status.SUCCESS);
		trigger.setFileSize(new Long(30));
		trigger.setProtocol(Trigger.Protocol.FTP);
		trigger.setMailbox("mailbox");
		trigger.setHost("host");
		trigger.setSourceFile("sourceFile");
		trigger.setSessionUser("sessionUser");
		trigger.setFileTimestamp(new Long(30));
		trigger.setTransmissionStarttime(new Long(30));
		trigger.setTransmissionTime(new Double(30));
		trigger.setTransmissionTransferid("TransmissionTransferid");
		return trigger;
	}

}
