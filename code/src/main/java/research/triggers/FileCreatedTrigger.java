package research.triggers;



import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/**
 * This class is a POJO for holding the trigger metadata
 * 
 * @author mohant
 *
 */
public class FileCreatedTrigger extends FileTrigger {

	// Session
	private String sessionId;
	private String sessionUser;

	// Transmission
	private String transmissionTransferid;
	private Long transmissionStarttime;
	private Double transmissionTime;

	private String host;
	private String mailbox;
	
	// Protocol
	private Protocol protocol;
	private String command;

	// Status
	private Status status;
	
	private String errorMessage;


	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}

	public String getTransmissionTransferid() {
		return transmissionTransferid;
	}

	public void setTransmissionTransferid(String transmissionTransferid) {
		this.transmissionTransferid = transmissionTransferid;
	}

	public long getTransmissionStarttime() {
		return transmissionStarttime;
	}

	public void setTransmissionStarttime(long transmissionStarttime) {
		this.transmissionStarttime = transmissionStarttime;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public String getSessionUser() {
		return sessionUser;
	}

	public void setSessionUser(String sessionUser) {
		this.sessionUser = sessionUser;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Double getTransmissionTime() {
		return transmissionTime;
	}

	public void setTransmissionTime(Double transmissionTime) {
		this.transmissionTime = transmissionTime;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getMailbox() {
		return mailbox;
	}

	public void setMailbox(String mailbox) {
		this.mailbox = mailbox;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String toString() {
		
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		String transStartTime = dateFormatGmt.format(new Date(transmissionStarttime));
		String fileTime = dateFormatGmt.format(new Date(fileTimestamp));

		StringBuffer sb = new StringBuffer("FileCreatedTrigger>>");
		sb.append("Id="+getUuid());
		sb.append("Status=" + status);
		sb.append(", SessionId=" + sessionId);
		sb.append(", SessionUser=" + sessionUser);
		sb.append(", FileName=" + sourceFile);
		sb.append(", FileSize=" + fileSize + " bytes");
		sb.append(", TimeStamp=" + fileTime);
		sb.append(", TransferId=" + transmissionTransferid);
		sb.append(", TransmissionStartTime="+  transStartTime);
		sb.append(", TrasmissionTime=" + transmissionTime + " secs");
		sb.append(", Protocol=" + protocol);
		sb.append(", Command=" + command);
		sb.append(", Host=" + host);
		sb.append(", Mailbox=" + mailbox);
		if (null != errorMessage && errorMessage.length() != 0) {
			sb.append(",Error=" + errorMessage);
		}
		return sb.toString();
		
	}
}
