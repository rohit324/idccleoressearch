/**
 * 
 */
package research.task;

import research.AkkaActor;

/**
 * @author ragarwal
 *
 */
public class BasicTask  implements Runnable, WorkerTask{
		private String name;
		String uuid;

		public BasicTask(String name, String uuid) {
			this.name = name;
			this.uuid = uuid;
		}

		public String getName() {
			return name;
		}
		

		public void run() {
			ComplexCalculator.doSomework();
		}
	}

