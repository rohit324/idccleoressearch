package research.task;

import research.AkkaActor;
import research.triggers.Trigger;
import simplepersistance.IEventPersistor;
import simplepersistance.TwoFilePersistor;

class ActualWorker implements WorkerTask {

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	int countValue;
	IEventPersistor persistor;
	Object event;
	
	public ActualWorker(int countValue, IEventPersistor persistor,Object event) {
		this.countValue = countValue;
		this.persistor = persistor;
		this.event = event;
	}

	@Override
	public void run() {
		ComplexCalculator.doSomework();
		try {
			if(event instanceof Trigger) {
				persistor.deleteMessage(((Trigger)event).getUuid());
			} else {
			persistor.deleteMessage(""+countValue);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}
}
