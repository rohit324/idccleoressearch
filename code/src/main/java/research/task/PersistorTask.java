/**
 * 
 */
package research.task;

import java.util.concurrent.atomic.AtomicInteger;

import research.AkkaActor;
import research.JDKThread;
import simplepersistance.IEventPersistor;
import simplepersistance.TwoFilePersistor;

import com.cleo.util.logger.XMLLogElement;

/**
 * @author ragarwal
 *
 */
public class PersistorTask implements WorkerTask{
	private String name;
	Object element;
	IEventPersistor persistor;
	public static AtomicInteger counter = new AtomicInteger();

	public PersistorTask(String name, Object element, IEventPersistor persist) {
		this.name = name;
		this.element = element;
		this.persistor = persist;
	}
	

	public String getName() {
		return name;
	}

	public void run() {
		final int countValue = counter.incrementAndGet();
		try {
			persistor.persistMessage(element,""+countValue);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		JDKThread.getWorkerPool().submit(new ActualWorker(countValue,persistor,element));
	}

}


	


