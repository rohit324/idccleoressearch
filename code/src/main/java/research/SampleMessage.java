package research;

import com.cleo.util.logger.XMLLogElement;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by sbelur on 23/10/15.
 */
public class SampleMessage {

    private String messageDetail;

    private XMLLogElement element;

    private int sid;

    private long start;
    private long startInMs;

    public SampleMessage(String messageDetail) {
        super();
        this.messageDetail = messageDetail;
        sid = ThreadLocalRandom.current().nextInt(100) % 5;
    }

    public SampleMessage(String messageDetail, XMLLogElement xmlLogElement) {
        this(messageDetail);
        this.element = xmlLogElement;
        //start = System.nanoTime();
    }

    public long getStartInMs() {
        return startInMs;
    }

    public void setStartInMs(long startInMs) {
        this.startInMs = startInMs;
    }

    public XMLLogElement getElement() {
        return element;
    }


    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setElement(XMLLogElement element) {
        this.element = element;
    }

    /**
     * @return the messageDetail
     */
    public String getMessageDetail() {
        return messageDetail;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return messageDetail;
    }

    public int getSid() {
        return sid;
    }


}
