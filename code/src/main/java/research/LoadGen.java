package research;

import akka.actor.ActorRef;
import com.cleo.util.logger.XMLLogElement;

import java.util.*;
import java.util.concurrent.*;

/**
 * Created by sbelur on 26/10/15.
 */
public class LoadGen {

    private final int tp;
    private BlockingQueue<SampleMessage> msgs;
    private ActorRef finalTarget;
    private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    public LoadGen(int total,int tp){
        msgs = new ArrayBlockingQueue<SampleMessage>(total);
        this.tp = tp;
        for(int ctr=1;ctr<=total;ctr++) {
            XMLLogElement msg = Events.generateEvent();
            SampleMessage s = new SampleMessage("", msg);
            msgs.add(s);
        }
    }

    public LoadGen(int total,int tp,ActorRef target){
        this(total,tp);
        this.finalTarget = target;
    }

    public void schedule(){
        List<SampleMessage> batch = new ArrayList<>();
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                //System.out.println("Current time "+new Date());
                int size = Math.min(tp,msgs.size());
                msgs.drainTo(batch,size);
                long start = System.currentTimeMillis();
                for(int c=0;c<batch.size();c++){
                    SampleMessage msg = batch.get(c);
                    msg.setStart(System.nanoTime());
                    finalTarget.tell(msg, ActorRef.noSender());
                }
                long end = System.currentTimeMillis();

                //System.out.println("Sent "+batch.size() + " messages to AKKA in "+ (end - start)+ "ms");
                batch.clear();
            }
        },0,1, TimeUnit.SECONDS);
    }


}
