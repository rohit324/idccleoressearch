/**
 * 
 */
package research;

import java.util.Map.Entry;
import java.util.Set;

import research.task.ComplexCalculator;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import com.typesafe.config.ConfigValueFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;

/**
 * @author ragarwal
 *
 */
public class AkkaConfigSample {
	
	public static void main(String[] args) {
//		propsConfig();
		printProps();
	}
	
	private static void propsConfig() {
		Config configFile = ConfigFactory.parseResources("research/test.props");
		ConfigFactory.load(configFile);
		ActorSystem system = ActorSystem.create("ConfigSystemINFO",configFile);
		for(int i=0;i<100000;i++) {
		ActorRef actor = system.actorOf(Props.create(ConfigActor.class),"Actor"+i);
		actor.tell(new ConfigSampleMessage("testDetail"), ActorRef.noSender());
		}
		Set<String> threadSet = ComplexCalculator.getThreadSet();
		system.shutdown();
		system.awaitTermination();
		System.out.println("\n expecting more than 550 threads.");
		System.out.println(threadSet);
		
	}
	
	private static void printProps() {
		Config configFile = ConfigFactory.parseResources("research/test.props");
		Config loadedConfig = ConfigFactory.load(configFile);
		Object anyRef = loadedConfig.getAnyRef("akka.actor.default-dispatcher.fork-join-executor.parallelism-factor");
		System.out.println(anyRef);
	}

	private static void staticConfig() {
		System.setProperty("ConfigSystemDEBUG.akka.loglevel", "DEBUG");
		System.setProperty("ConfigSystemINFO.akka.loglevel", "INFO");
		Config config = ConfigFactory.load();
		ActorSystem system = ActorSystem.create("ConfigSystemINFO",config.getConfig("ConfigSystemINFO"));
		system.shutdown();
		system = ActorSystem.create("ConfigSystemDEBUG",config.getConfig("ConfigSystemDEBUG"));
		system.shutdown();
	}
}


class ConfigActor extends UntypedActor {

		/* (non-Javadoc)
	 * @see akka.actor.UntypedActor#onReceive(java.lang.Object)
	 */
	@Override
	public void onReceive(Object paramObject) throws Exception {
		ComplexCalculator.addThreadToSet(Thread.currentThread().getName());
	}

}

class ConfigSampleMessage {
	
	private String detail;

	public ConfigSampleMessage(String detail) {
		super();
		this.detail = detail;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	
}
