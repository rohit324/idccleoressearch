package research.H2Database;

import java.util.concurrent.atomic.AtomicInteger;


import research.task.WorkerTask;

public class H2PersistorTask implements WorkerTask {

    public static AtomicInteger counter = new AtomicInteger();
    
	public H2PersistorTask() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		final int countValue = counter.incrementAndGet();
		try {
			H2Database persistor = new H2Database();
			persistor.addTriggerToStore("" + countValue);
			persistor.triggerDone("" + countValue);
			persistor.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
