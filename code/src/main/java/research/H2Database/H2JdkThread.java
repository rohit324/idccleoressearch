package research.H2Database;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import research.Tester;
import research.task.WorkerTask;

public class H2JdkThread {

	static LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>();
	static int total = Runtime.getRuntime().availableProcessors() * 2;

	static {
		String totalThread = System
				.getProperty(Tester.PROP_NAME_JDK_THREADS_COUNT);
		if (totalThread != null) {
			total = Integer.parseInt(totalThread);
			System.out.println("Prop set Total threads used for execution are "
					+ total);
		} else {
			System.out
					.println("Prop not set Total threads used for execution are "
							+ total);
		}
	}

	/*static ExecutorService pool = new ThreadPoolExecutor(total, total, 600L,
			TimeUnit.SECONDS, workQueue);
*/
	/*public static ExecutorService getWorkerPool() {
		return pool;
	}*/

	public static void executeUsingH2Database() throws Exception {
		ExecutorService pool = new ThreadPoolExecutor(total, total, 600L,
				TimeUnit.SECONDS, workQueue);
		WorkerTask task = null; // new BasicTask("testing", null);
		task = new H2PersistorTask();
		for (int i = 0; i < Tester.totalTaskCount; i++) {
			pool.execute(task);
		}
		
		pool.shutdown();
		pool.awaitTermination(80000, TimeUnit.SECONDS);
		System.out.println("Pool shutdown!!");
		/*H2Database h2Data = new H2Database();
		h2Data.printRows();
		System.out.println("Number of rows : " );
		//h2Data.removeAllRows();
		h2Data.close();*/

	}
}
