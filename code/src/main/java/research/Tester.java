/**
 *
 */
package research;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import research.H2Database.H2Database;
import research.H2Database.H2JdkThread;
import research.serversimulator.ClientTest;
import research.serversimulator.ServerTest;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.FiniteDuration;
import simplepersistance.ConcurrentEventPersistor;
import simplepersistance.JsonFilePersistor;
import simplepersistance.TwoFilePersistor;
import simplepersistance.SingleFilePersistor;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * @author ragarwal
 */
public class Tester {

    public static enum Mode {
        actors,
        akka_futures,
        jdk_threads,
        jdk_threads_persistance,
        jdk_threads_persistance_single_file,
        jdk_threads_concurrent_persistance,
        h2Database,
        jdk_threads_persistence_single_file_json
    }

    public static String EXECUTION_MODE = "mode";
    public static String PROP_NAME_JDK_THREADS_COUNT = "jdkthreads.total";
    public static String PROP_NAME_PERSIST = "persist";
    public static String PROP_NAME_EXE_TYPE = "exeType";
    public static String PROP_NAME_PERSIST_STRATEGY = "PERSIST_STRATEGY";

    public static enum EXECUTION_TYPE {
        SYNC, ASYNC
    }

    public static int totalTaskCount = 0;
    public static int initialCount = 1000;
    public static int statisticCollectionLoopCounts = 10;
    public static FileOutputStream stream = null;
    public static final String KEY_NAME_TASKS = "Tasks";
    public static final String KEY_NAME_TIME = "Time";
    private static final String ServerTest = "ServerTest";

    public static LinkedHashMap<String, LinkedList<String>> statsMap = new LinkedHashMap<String, LinkedList<String>>();
    public static ArrayList<Double> cpuData = new ArrayList<Double>();
    public static ArrayList<Double> cpuMEM = new ArrayList<Double>();

    private static void printUsage() throws Exception {
        OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
        Method method = operatingSystemMXBean.getClass().getMethod("getProcessCpuLoad", null);
        method.setAccessible(true);
        Method memMethod = operatingSystemMXBean.getClass().getMethod("getFreePhysicalMemorySize", null);
        memMethod.setAccessible(true);
        while (true) {
            Object value = method.invoke(operatingSystemMXBean);
            double cpuLoad = Double.parseDouble(value.toString());
            cpuData.add(cpuLoad);

            Object memValue = memMethod.invoke(operatingSystemMXBean);
            cpuMEM.add(Double.parseDouble(memValue.toString()));
            Thread.sleep(100);
        }
    }

    static ExecutorService systemMonitor = Executors.newSingleThreadExecutor();
    private static Object pid;

    public static void main(String[] args) throws Exception {
        System.out.println("running now");
        performTestForPersistance();
        JDKThread.getWorkerPool().shutdown();
    }

	private static void runH2() throws Exception {
		H2Database conToH2 = new H2Database();
        conToH2.removeAllRows();
        //killServer();
       // Process process = startServerSimulator();
        System.setProperty(PROP_NAME_PERSIST_STRATEGY, TwoFilePersistor.PERSISTANCE_TYPE.SINGLUAR.toString());
       // performTest();
        performTestFileAndH2();
//		testPersist();
	}

    public static void killServer() throws IOException {
        Process process = Runtime.getRuntime().exec("jps");
        BufferedReader stdInput = new BufferedReader(new InputStreamReader(
                process.getInputStream()));
        String output = null;
        while ((output = stdInput.readLine()) != null) {
            if (output.endsWith(ServerTest)) {
                pid = output.substring(0, output.indexOf(" "));
                break;
            }
        }
        if (pid != null) {
            if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1)
                Runtime.getRuntime().exec("taskkill /f /pid " + pid);
            else
                Runtime.getRuntime().exec("kill -9 " + pid);
        }
    }

    private static Process startServerSimulator() throws IOException {
        URL resource = ClientTest.class.getResource(".");
        //System.out.println(resource);
        String classPath = resource.getFile();
        classPath = classPath.replace("research/serversimulator/", "");
        System.out.println(classPath);
        //System.out.println(classPath);
        return Runtime.getRuntime().exec("java -cp " + classPath + " research.serversimulator.ServerTest");
    }

    public static HashMap<String, String> stopMonitoring() {
        HashMap<String, String> metricMap = new HashMap<String, String>();
        double cpuValue = getAverageValue(cpuData);
        metricMap.put("CPU", cpuValue + "");
        double memValue = getAverageValue(cpuMEM);
        metricMap.put("MEM", memValue + "");
        System.out.println("\n++++++\nAverage CPU load = " + cpuValue + "\n+++++++\n");
        systemMonitor.shutdownNow();
        cpuData = new ArrayList<Double>();
        //systemMonitor = Executors.newSingleThreadExecutor();
        return metricMap;
    }

    private static double getAverageValue(ArrayList<Double> list) {
        double value = 0;
        for (Double double1 : list) {
            value = value + double1;
        }
        value = value / list.size();
        return value;
    }

    public static void startMonitoring() {
    	systemMonitor = Executors.newSingleThreadExecutor();
        systemMonitor.execute(
                new Runnable() {

                    @Override
                    public void run() {
                        try {
                            printUsage();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            //e.printStackTrace();
                        }

                    }
                });
    }

    private static void testPersist() throws Exception {
        System.out.println("***collecting stats for jdk threads with Persistance***.");
        System.setProperty(PROP_NAME_PERSIST, "true");
        System.setProperty(PROP_NAME_PERSIST_STRATEGY, TwoFilePersistor.PERSISTANCE_TYPE.CONCURRENT.toString());
        String executionMode = Mode.jdk_threads.toString();
        System.setProperty(EXECUTION_MODE, executionMode);
        startMonitoring();
        collectStatsForExecutionMode(Mode.jdk_threads_persistance.toString(), initialCount);
        HashMap<String, String> metricMap = stopMonitoring();
        String jdkCPUPersistUsage = metricMap.get("CPU");
        String jdkMEMPersist = metricMap.get("MEM");
        stopMonitoring();
    }
    
    private static void performTestForPersistance() throws Exception {
    	
        HashMap<String, String> metricMap = null;

        metricMap = JsonFilePersistor.performTestSingularPersistJsonFormat();
        String jdkCPUJSONPersistUsage = metricMap.get("CPU");
        String jdkMEMJSONPersist = metricMap.get("MEM");

        metricMap = TwoFilePersistor.performTestSingularPersist();
        String jdkCPUPersistUsage = metricMap.get("CPU");
        String jdkMEMPersist = metricMap.get("MEM");
        
        metricMap = SingleFilePersistor.performTestSingularPersist();
        String singleFileCPU = metricMap.get("CPU");
        String singleFileMEM = metricMap.get("MEM");
         
         dumpStats();
         String data = "Average CPU Usage\t" + jdkCPUJSONPersistUsage + "\t" + jdkCPUPersistUsage+"\t" + singleFileCPU +"\n"
                 + "\n";
         stream.write(data.getBytes());
         data = "Free Physical Memory\t" +  jdkMEMJSONPersist + "\t" + jdkMEMPersist+"\t" + singleFileMEM +"\n";
         stream.write(data.getBytes());
         closeStats();

    }

    private static void performTest() throws Exception {
        JDKThread.configureSameNumberJDKThreadsAsForAkka();
        HashMap<String, String> metricMap = null;
        metricMap = AkkaActor.performTestAkka();
        String akkaCPUUsage = metricMap.get("CPU");
	    String akkaMEM = metricMap.get("MEM");

        Thread.sleep(10000);
       metricMap = JDKThread.performTestJdkThreads();
       String jdkCPUUsage = metricMap.get("CPU");
        String jdkMEM = metricMap.get("MEM");

        metricMap = TwoFilePersistor.performTestSingularPersist();
        String jdkCPUPersistUsage = metricMap.get("CPU");
        String jdkMEMPersist = metricMap.get("MEM");
        
        metricMap = H2Database.performTestSingularPersist();
        String h2DatabaseCPUUsage = metricMap.get("CPU");
        String h2DatabaseMEM = metricMap.get("MEM");

        
        metricMap = ConcurrentEventPersistor.performTestConcurrentPersist();
        String jdkCPUPersistConUsage = metricMap.get("CPU");
        String jdkMEMPersistCon = metricMap.get("MEM");

        dumpStats();
        String data = "Average CPU Usage\t" +  akkaCPUUsage +"\t"+jdkCPUUsage+"\t" + jdkCPUPersistUsage+"\t" + jdkCPUPersistConUsage +"\n"
                + "\n";
        stream.write(data.getBytes());
        data = "Free Physical Memory\t" +  akkaMEM +"\t"+jdkMEM +"\t" + jdkMEMPersist+"\t" + jdkMEMPersistCon +"\n";
        stream.write(data.getBytes());
        closeStats();
        

    }
    
    private static void performTestFileAndH2() throws Exception {
        
    	HashMap<String, String> metricMap = null;
        metricMap = H2Database.performTestSingularPersist();
        String h2DatabaseCPUUsage = metricMap.get("CPU");
        String h2DatabaseMEM = metricMap.get("MEM");

        
        dumpStatsForFileAndH2();
        String data = "Average CPU Usage\t" +  h2DatabaseCPUUsage
                + "\n";
        stream.write(data.getBytes());
        data = "Free Physical Memory\t" +  h2DatabaseMEM +"\n";
        stream.write(data.getBytes());
        closeStats();

    }


    private static void dumpStats() throws Exception {
        LinkedHashMap<String, LinkedList<String>> dumpMap = new LinkedHashMap<String, LinkedList<String>>();
        File file = new File("stats.xls");
        stream = new FileOutputStream(file);
        Set<String> keySet = statsMap.keySet();
        String header = "Tasks\t";
        for (String mode : keySet) {
        	header = header+mode+"\t";
		}
        stream.write((header+"\n").getBytes());

        LinkedList<String> taskKey = statsMap.values().iterator().next();
        for (int i = 0; i < taskKey.size(); i++) {
            String taskEntry = taskKey.get(i);
            dumpMap.put(taskEntry.split("=")[0], new LinkedList<String>());
        }

        for (Map.Entry<String, LinkedList<String>> entry : statsMap.entrySet()) {
            // System.out.println(entry.getKey() + "/" + entry.getValue());
            for (String item : entry.getValue()) {
                // print the map's key with each value in the ArrayList
                //System.out.println(entry.getKey() + ": " + item);
                String dumpKey = item.split("=")[0];
                String dumpValue = item.split("=")[1];
                if (dumpMap.get(dumpKey) != null)
                    dumpMap.get(dumpKey).add(dumpValue);
            }
        }

        for (Map.Entry<String, LinkedList<String>> entry : dumpMap.entrySet()) {
            String data = entry.getKey();
            for (String item : entry.getValue()) {
                data = data + "\t" + item;
            }
            data = data + "\n";
            stream.write(data.getBytes());
        }
    }
    
    private static void dumpStatsForFileAndH2() throws Exception {
        LinkedHashMap<String, LinkedList<String>> dumpMap = new LinkedHashMap<String, LinkedList<String>>();
        File file = new File("stats.xls");
        stream = new FileOutputStream(file);
        String header = "Tasks\tH2Database (ms)\tFile Persistance(ms)\n";
        stream.write(header.getBytes());

        LinkedList<String> taskKey = statsMap.get(Mode.h2Database.toString());
        for (int i = 0; i < taskKey.size(); i++) {
            String taskEntry = taskKey.get(i);
            dumpMap.put(taskEntry.split("=")[0], new LinkedList<String>());
        }

        for (Map.Entry<String, LinkedList<String>> entry : statsMap.entrySet()) {
            // System.out.println(entry.getKey() + "/" + entry.getValue());
            for (String item : entry.getValue()) {
                // print the map's key with each value in the ArrayList
                //System.out.println(entry.getKey() + ": " + item);
                String dumpKey = item.split("=")[0];
                String dumpValue = item.split("=")[1];
                if (dumpMap.get(dumpKey) != null)
                    dumpMap.get(dumpKey).add(dumpValue);
            }
        }

        for (Map.Entry<String, LinkedList<String>> entry : dumpMap.entrySet()) {
            String data = entry.getKey();
            for (String item : entry.getValue()) {
                data = data + "\t" + item;
            }
            data = data + "\n";
            stream.write(data.getBytes());
        }
    }

    private static void closeStats() throws Exception {
        stream.close();
    }

    public static void collectStatsForExecutionMode(String executionMode,
                                                    int initialCount) throws Exception {

        LinkedList<String> detailsList = statsMap.get(executionMode);
        if (detailsList == null) {
            detailsList = new LinkedList<String>();
            statsMap.put(executionMode, detailsList);
        }

        long startTime = System.currentTimeMillis();
        runWarmUp(initialCount);
        long endTime = System.currentTimeMillis();
        System.out.println(" Time taken by  " + executionMode + " for warm up = " + ((endTime - startTime) / statisticCollectionLoopCounts) + " milli seconds.");


        System.out.println("Collecting stats for Average case.");
        startTime = System.currentTimeMillis();
        runAverageConstantCount(initialCount);
        endTime = System.currentTimeMillis();
        long totalTimeForAverageExec = (endTime - startTime) / statisticCollectionLoopCounts;
        int totalNumber = initialCount;
        System.out.println(" ----------- Time taken by  " + executionMode + " for average case = " + totalTimeForAverageExec + " milli seconds.");


        detailsList.add(totalNumber + "=" + totalTimeForAverageExec);


        System.out.println("Collecting stats for Incremental case.");
        startTime = System.currentTimeMillis();
        int totalIncrementalTasks = runIncremental(initialCount, executionMode, detailsList);
        endTime = System.currentTimeMillis();
        long totalIncrementalTime = endTime - startTime;
        detailsList.add(totalIncrementalTasks + "=" + totalIncrementalTime);
        System.out.println(" -------- Total Time taken by  " + executionMode + " for incremental = " + totalIncrementalTime + " milli seconds.");
    }


    private static int runIncremental(int initialCount, String executionMode, LinkedList<String> detailsList)
            throws Exception {
        int taskCount = 0;
        for (int i = 1; i <= statisticCollectionLoopCounts; i++) {
            long startTime = System.currentTimeMillis();
            totalTaskCount = initialCount * i * 10;
            taskCount = totalTaskCount + taskCount;
            startExecution();
            long timeTaken;
            long endTime = System.currentTimeMillis();
            timeTaken = endTime - startTime;
            //System.out.println(" Time taken by  " + executionMode + " for incremental = " + timeTaken + " milli seconds.");

            detailsList.add(totalTaskCount + "=" + timeTaken);
        }
        return taskCount;
    }

    private static void runAverageConstantCount(int initialCount)
            throws Exception {
        for (int i = 1; i <= statisticCollectionLoopCounts; i++) {
            totalTaskCount = initialCount;
            startExecution();
        }
    }


    private static void runWarmUp(int initialCount)
            throws Exception {
        for (int i = 1; i <= statisticCollectionLoopCounts; i++) {
            totalTaskCount = initialCount;
            System.out.println("\n\n running warm up\n\n");
            startExecution();
            AKKA.getTaskBarrier().reset();
        }
    }

    public static boolean getPersistProperty() {
        String persist = System.getProperty(PROP_NAME_PERSIST);
        if ("true".equalsIgnoreCase(persist)) {
            return true;
        }
        return false;
    }

    private static void startExecution() throws Exception {
        Mode mode = null;
        mode = Mode.valueOf(System.getProperty(EXECUTION_MODE));

        boolean persist = getPersistProperty();

        if (mode == null)
            throw new IllegalArgumentException("specify mode as program argument to be one of " + Mode.values());

        switch (mode) {
            case actors:
                AkkaActor.executeUsingAkkaActors(persist);
                break;
            case akka_futures:
                AkkaActor.executeUsingAkkaFutures();
                break;
            case jdk_threads:
                JDKThread.executeUsingJDKExecutors();
                break;
            case h2Database:
                H2JdkThread.executeUsingH2Database();
                break;
            default:
        }
    }


}
