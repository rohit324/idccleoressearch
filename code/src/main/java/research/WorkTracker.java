package research;

import akka.actor.UntypedActor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sbelur on 18/10/15.
 */
public class WorkTracker extends UntypedActor {

    private int workDone = 0;
    private long startTime = 0;
    private boolean recoveryDone = false;
    private List<Long> allTimes = new ArrayList<>();

    //private Histogram histogram = new Histogram(3600000000000L, 5);

    @Override
    public void onReceive(Object message) throws Exception {

        if("start".equals(message)){
            startTime = System.currentTimeMillis();
            //System.out.println("startTime "+startTime);
            recoveryDone = true;
            workDone = 0;
            //System.out.println("started in tracker");
            //allTimes.clear();
           //histogram.reset();
            //System.out.println(startTime);
        }
        else if("resetTimes".equals(message)){
            allTimes.clear();
        }


        else if(message instanceof WorkDone) {
            workDone++;
           /*if(((WorkDone) message).getStart() > 0) {
                if(recoveryDone) {
                    long value = ((WorkDone) message).getEnd() - ((WorkDone) message).getStart();
                    //System.out.println("recorded value "+value);
                    histogram.recordValue(value);
                }
            }*/
            //System.out.println(workDone);
            //System.out.println("in tracker "+workDone+ " , "+ActorSample.totalTaskCount);
            if(recoveryDone && workDone == Tester.totalTaskCount){
                long end = System.currentTimeMillis();
                allTimes.add(end-startTime);
                //histogram.outputPercentileDistribution(System.out, 1000.0);

                //System.out.println("Total work done in work tracker " + workDone);
                //System.out.println("endtime "+end);

                System.out.println("Total time taken " + (end - startTime) + " ms");
               //System.out.println("        taskBarrier.getNumberWaiting() "+   AKKA.getTaskBarrier().getNumberWaiting());
                AKKA.getTaskBarrier().await();
                //System.out.println("        Now taskBarrier.getNumberWaiting() " + AKKA.getTaskBarrier().getNumberWaiting());

                //getContext().stop(getSelf());
            }
        }

        else if(message.equals("alltimes")){
            getSender().tell(allTimes,getSelf());
        }
    }

   /* @Override
    public void postStop() throws Exception {
        System.out.println("Total work done in work tracker "+workDone);
        System.out.println("Total time taken " + (System.currentTimeMillis() - startTime) + " ms");
        ActorSample.taskBarrier.await();
        //getContext().system().shutdown();
    }*/
    
} 
