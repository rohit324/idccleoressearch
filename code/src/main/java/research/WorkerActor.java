package research;

import research.task.ComplexCalculator;
import akka.actor.ActorSelection;
import akka.actor.UntypedActor;

import com.cleo.labs.trigger.persist.akka.events.ConfirmationLexEvent;
import com.cleo.labs.trigger.persist.akka.messages.ConfirmEventExecution;

/**
 * Created by sbelur on 18/10/15.
 */
public class WorkerActor extends UntypedActor {

	private String identifier;


	public WorkerActor(String identifier) {
		super();
		this.identifier = identifier;
	}

    public WorkerActor(){}

	/* (non-Javadoc)
	 * @see akka.actor.UntypedActor#onReceive(java.lang.Object)
	 */
	@Override
	public void onReceive(Object paramObject) throws Exception {
        //System.out.println(getSelf().path());
        if(paramObject instanceof SampleMessage) {
           ComplexCalculator.doSomeAkkawork();
 //           getSender().tell(WorkDone.getINSTANCE(),getSelf());
   //         doTimeConsumingTask();
            getSender().tell(new WorkDone(0L,0L),getSelf());

		}
        else if(paramObject instanceof PersTestEvent){
            long end = System.nanoTime();
            //long startInMs = ((PersTestEvent) paramObject).getStartInMs();
         /*   if(startInMs > 0) {
                long needed = System.currentTimeMillis() - startInMs;
                if (needed > 0)
                    System.out.println("Time needed " + needed);
            }*/

           /*long l = System.currentTimeMillis() - ((PersTestEvent) paramObject).getTimeSentToWorker();
            if(l > 1)
                System.out.println("Latency time "+ l);*/

            PersTestEvent msg = (PersTestEvent)paramObject;

            long startc = System.currentTimeMillis();
            ComplexCalculator.doSomeAkkawork();
           long ct = System.currentTimeMillis() - startc;
            if(ct > 1)
                System.out.println("compute time " + ct);
            Object deliveryId = msg.getDeliveryId();
            if(deliveryId == null){
                deliveryId = msg.getDeliveryIdStr();
            }
            getSender().tell(new ConfirmEventExecution(deliveryId, ((PersTestEvent) paramObject).getStart(), end), getSelf());

            //TODO - remove this post poc
            if (System.getProperty("persist") != null) {
                ActorSelection sel = getContext().system().actorSelection("/user/tracker");
                sel.tell(new WorkDone(((PersTestEvent) paramObject).getStart(), end), getSelf());
                //deleteMessage(((ConfirmationLexEvent) event).getEventDeliveryId());
            }

        }
		else unhandled(paramObject);

	}


    private void doTimeConsumingTask(){
        int i;
        for(i=0;i<1000000;i++) {
            for(int j=0;j<10000;j++) {
                float comlexCal = 676764^676^676*898/67*4^7^7*89;
                comlexCal = 676764^676^676*898/67*4^7^7*89;
                for(int k=0;k<1000;k++) {
                    comlexCal = 676764^676^676*898/67*4^7^7*89;
                    comlexCal = 676764^676^676*898/67*4^7^7*89;
                }
            }
        }
        //System.out.println("Done time task "+i);
    }
}
