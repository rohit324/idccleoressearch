/**
 * 
 */
package misc.collections;

import java.util.Vector;

import com.cleo.lexicom.Schedule.Item;

/**
 * @author ragarwal
 *
 */
public class TestListForCustomVector extends TestListWithIteration{

  public static void main(String[] args) throws Exception {
    dumpStats();
    long startTime = System.currentTimeMillis();
    int testCounter = 10;
    for(int i=0;i<testCounter;i++) {
      new TestListForCustomVector().performTest();
    }
    long endTime = System.currentTimeMillis();
    System.out.println("Time Taken = " + (endTime-startTime)/testCounter);
    dumpStats();
  }

  public  void performTest() throws Exception {
    MyVector<Item> vector = new MyVector<Item>();
    populateVector(vector);
    for(int i=0;i<10;i++) {
      vector.getTriggers();
    }
  }
}
