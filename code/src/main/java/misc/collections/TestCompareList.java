/**
 * 
 */
package misc.collections;

/**
 * @author ragarwal
 *
 */
public class TestCompareList {
  
  public static void main(String[] args) throws Exception {
    System.out.println("warming up the system.");
    for(int i=0;i<5;i++) {
    getStats(args);
    }
    
    int testCycles = 10;
    
    for(int i=TestListWithIteration.itemCount;i<TestListWithIteration.itemCount*200;i=i*5) {
    System.out.println("\n Running test item counts = " + i + ". \n");
    runTest(testCycles,new TestListWithIteration());
    TestListForCustomVector.dumpStats();
    runTest(testCycles,new TestListForJava8Filtering());
    TestListForCustomVector.dumpStats();
    runTest(testCycles,new TestListForCustomVector());
    TestListForCustomVector.dumpStats();
    }
  }

  private static void runTest(int testCycles, TestListWithIteration instance) throws Exception {
    System.out.println("######## Stats with  " + instance.getClass().getName() + " ######## \n");
    long startTime = System.currentTimeMillis();
    for (int i=0;i<testCycles;i++) {
    instance.performTest();
    }
    long endTime = System.currentTimeMillis();
    System.out.println("Time take = " + (endTime-startTime)/testCycles);
  }

  private static void getStats(String[] args) throws Exception {
    new TestListForCustomVector().performTest();
    new TestListWithIteration().performTest();
    new TestListForJava8Filtering().performTest();
  }

}
