/**
 * 
 */
package misc.collections;

import java.util.Collection;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.Vector;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;


/**
 * @author ragarwal
 *
 */
public class MyVector<E> extends Vector<E>{

  private Vector<Object> triggers = new Vector<Object>();

  public Vector<Object> getTriggers() {
    return triggers;
  }
  /* (non-Javadoc)
   * @see java.util.Vector#addElement(java.lang.Object)
   */
  @Override
  public synchronized void addElement(E obj) {
    // TODO Auto-generated method stub
    com.cleo.lexicom.Schedule.Item item = (com.cleo.lexicom.Schedule.Item)obj;
    super.addElement(obj);
    if(item.getItemTrigger()!=null) {
      triggers.addElement(obj);
    }

  }
  

  /* (non-Javadoc)
   * @see java.util.Vector#add(java.lang.Object)
   */
  @Override
  public synchronized boolean add(E e) {
    if(((com.cleo.lexicom.Schedule.Item)e).getItemTrigger()!=null) {
      triggers.add(e);
    }
    return super.add(e);
   

  } 

  
}
