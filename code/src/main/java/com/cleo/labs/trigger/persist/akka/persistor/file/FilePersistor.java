package com.cleo.labs.trigger.persist.akka.persistor.file;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.dispatch.ExecutionContexts;
import akka.dispatch.MessageDispatcher;
import akka.routing.BalancingPool;
import akka.routing.RoundRobinPool;
import akka.routing.SmallestMailboxPool;
import com.cleo.labs.trigger.persist.akka.messages.ConfirmEventExecution;
import com.cleo.labs.trigger.persist.akka.messages.MapInput;
import com.cleo.util.logger.XMLLogElement;
import research.*;
import scala.concurrent.Future;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.*;
import java.util.concurrent.Callable;

import static akka.dispatch.Futures.future;

/**
 * Created by sbelur on 23/10/15.
 */
public class FilePersistor extends UntypedActor {

    private final String sessionId;

    public static final int PAGE_SIZE = 1024 * 4;
    public static final long FILE_SIZE = PAGE_SIZE *  100000L ;
    public static final byte[] BLANK_PAGE = new byte[PAGE_SIZE];


    private Map<String, MapInput> unacked = new HashMap<String, MapInput>();

    //private FileOutputStream file;
    private RandomAccessFile file;

    //private Schema schema;

    private ActorRef workerRouterRef;

    private String fileName;

    @Override
    public void preStart() throws Exception {
        super.preStart();
        //System.out.println(AKKA.getSystem().dispatcher());//AKKA.getSystem().dispatchers().lookup("akka.actor.pinnedWorkerDispatcher");
        openStore();
        recover();
    }
    File fileRef;

    private void openStore() throws IOException {
        fileRef= new File("events" + sessionId + ".txt");
        this.file = new RandomAccessFile(fileRef,"rw");
        //schema = new Schema.Parser().parse(getClass().getResourceAsStream("/event.avsc"));
    }

    private void recover() {

    }

    private boolean firstMsg = false;


    public FilePersistor(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof SampleMessage) {

            handleEvent((SampleMessage) message);

        } else if (message instanceof ConfirmEventExecution) {
            confirm((ConfirmEventExecution) message);
        } else unhandled(message);
    }

    private void handleEvent(SampleMessage message) {
        XMLLogElement xml = message.getElement();
           /* if (!firstMsg) {
                firstMsg = true;
                ActorSelection sel = getContext().system().actorSelection("/user/tracker");
                sel.tell("start", getSelf());
            }*/
        String deliveryIdStr = UUID.randomUUID().toString();

        Map<String, Serializable> map = new HashMap<>();
        map.put("left", "1");
        map.put("right", "2");
        map.put("done", "false");
        map.put("did", deliveryIdStr);
        MapInput mapInput = new MapInput(map);
        //long sp = System.currentTimeMillis();
        boolean done = writeWithFileChannel(mapInput);
            /*long l = System.currentTimeMillis() - sp;
            if (l > 10)
                System.out.println("To persist " + l);
            */
        PersTestEvent persTestEvent = new PersTestEvent("", message.getStart(), message.getStartInMs());
        persTestEvent.setDeliveryIdStr(deliveryIdStr);
        persTestEvent.setTimeSentToWorker(System.currentTimeMillis());
        //workerRouterRef.tell(persTestEvent, getSelf());
        //unacked.put(UUID.randomUUID().toString(), mapInput);

        getSender().tell(done,getSelf());

        future(new Computation(persTestEvent, getSelf()), AKKA.getSystem().dispatcher());
    }

    private void confirm(ConfirmEventExecution message) {
        unacked.remove(message.getDeliveryId());
        Map<String, Serializable> map = new HashMap<>();
        map.put("done", "true");
        map.put("did", message.getDeliveryId().toString());
        MapInput mapInput = new MapInput(map);
        writeWithFileChannel(mapInput);
    }


    private boolean writeWithFileChannel(MapInput mapInput) {
        boolean done = true;
        try {
           /*  GenericRecord event = new GenericData.Record(schema);
            //event.put("sessionid", sessionId);
            if (mapInput.getKey("left") != null)
                event.put("left", mapInput.getKey("left"));
            else
                event.put("left", "");

            if (mapInput.getKey("right") != null)
                event.put("right", mapInput.getKey("right"));
            else
                event.put("right", "");

            event.put("done", mapInput.getKey("done"));
            event.put("did", mapInput.getKey("did"));
*/

            // Serialize it.
            //ByteArrayOutputStream out = new ByteArrayOutputStream();
            /*DatumWriter<GenericRecord> writer = new GenericDatumWriter<GenericRecord>(schema);
            Encoder encoder = EncoderFactory.get().binaryEncoder(out, null);
            */

            //writer.write(event, encoder);

            //encoder.flush();
            //out.close();

            byte[] content = ("name=" + System.currentTimeMillis() + ";did=" + mapInput.getKey("did") + "\n").getBytes();
            //file.seek(fileRef.length());
            file.write(content);
            //file.getChannel().force(false);
            //out.toByteArray();

            // move the cursor to the end of the file
            //file.seek(file.length());

       /*     // obtain the a file channel from the RandomAccessFile
            FileChannel fileChannel = file.getChannel();

            ByteBuffer buf = ByteBuffer.allocate(1024);
            buf.clear();
            buf.putInt(content.length);
            buf.put(content);

            buf.flip();

            while (buf.hasRemaining()) {
                fileChannel.write(buf);
            }*/


        } catch (Exception e) {
            e.printStackTrace();
            done = false;
        }
        return done;
    }

    private static class Computation implements Callable<Void> {

        private final PersTestEvent paramObject;
        private final ActorRef sender;


        public Computation(PersTestEvent persTestEvent, ActorRef sender) {
            this.paramObject = persTestEvent;
            this.sender = sender;
        }

        public Void call() throws Exception {
            long s = System.currentTimeMillis();
            long d = paramObject.getTimeSentToWorker();
            /*long tm = s - d;
            if(tm > 100){
                System.out.println("to future "+ tm);
            }*/
            long end = System.nanoTime();
            int i;
            for (i = 0; i < 1000000; i++) {
                for (int j = 0; j < 10000; j++) {
                    float comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                    comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                    for (int k = 0; k < 1000; k++) {
                        comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                        comlexCal = 676764 ^ 676 ^ 676 * 898 / 67 * 4 ^ 7 ^ 7 * 89;
                    }
                }
            }
            sender.tell(new ConfirmEventExecution(paramObject.getDeliveryIdStr(), paramObject.getStart(), end), ActorRef.noSender());

            ActorSelection sel = AKKA.getSystem().actorSelection("/user/tracker");
            sel.tell(new WorkDone(((PersTestEvent) paramObject).getStart(), end), ActorRef.noSender());
            //deleteMessage(((ConfirmationLexEvent) event).getEventDeliveryId());
            return null;
        }
    }
}
