package com.cleo.labs.trigger;

import com.cleo.lexicom.external.*;
import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by sbelur on 20/09/15.
 */
public abstract class AbstractEventTrigger implements LexiComLogListener {
    protected ILexiCom lexicom  = null;


    public AbstractEventTrigger(){
        try {
            lexicom = LexiComFactory.getCurrentInstance();
        } catch (Exception ignore) {
        }
    }

    /**
     * Maps session types to protocol numbers.
     */
    private static final Map<String,Integer> PROTOCOL = new HashMap<String,Integer>();
    static {
        PROTOCOL.put("FTP", ILexiCom.FTP_SERVER);
        PROTOCOL.put("SSH FTP", ILexiCom.SSHFTP_SERVER);
    }


    private static final Map<String, Pattern> LOGIN_RESPONSE = new HashMap<String, Pattern>();

    static {
        LOGIN_RESPONSE.put("SSH FTP", Pattern.compile("SSH_MSG_USERAUTH_SUCCESS - User (.*) logged in .*"));
        LOGIN_RESPONSE.put("FTP", Pattern.compile("230 User (.*) logged in .*"));
    }

    /**
     * Keeps track of active (or pending) {@code Session}s
     * indexed by {@code id}.
     */
    private Map<String, Session> sessions = new HashMap<String, Session>();

    /**
     * Finds the local host of type {@code type} for user {@code mailbox}.
     * @param type the session type (protocol)
     * @param mailbox the username
     * @return the local host name or {@code null} if one is not found
     */
    protected String find_host(String type, String mailbox) {
        try {
            if (AbstractEventTrigger.PROTOCOL.containsKey(type)) {
                int protocol = AbstractEventTrigger.PROTOCOL.get(type);
                for (String host : lexicom.list(ILexiCom.HOST, new String[0])) {
                    if (lexicom.getHostProtocol(host)==protocol) {
                        for (String alias : lexicom.list(ILexiCom.MAILBOX, new String[] {host})) {
                            if (alias.equals(mailbox)) {
                                return host;
                            }
                        }
                    }
                }
            }
        } catch (Exception ignore) {}
        return null;
    }

    /**
     * The {@code Session} class keeps track of information related to
     * pending or active login sessions for FTP and SFTP.
     */
    protected class Session {
        private String  id;         // session ID: the CommandID in the related events
        private String  type;       // session type: "FTP" or "SSH FTP"
        private String  mailbox;    // session mailbox
        private String  host;       // session host
        private boolean login;      // has a valid login been seen?
        /**
         * Create a new {@code Session} identified by {@code id}.  The session
         * {@code type}, {@code mailbox}, and {@code login} status will be
         * parsed from subsequent events.
         * @param id the command ID identifying the session
         */
        public Session(String id) {
            this.id      = id;
            this.type    = null;
            this.mailbox = null;
            this.host    = null;
            this.login   = false;
        }
        /**
         * Parses a &lt;Request&gt; event looking for the identifying
         * {@code type} and {@code mailbox} attributes, if they have
         * not been set already.
         * @param event the event to parse
         */
        public void parseRequest(Element event) {
            // <Request text="PASS **********" type="FTP" mailbox="mailbox@host"></Request>
            // <Request text="SSH_MSG_USERAUTH_REQUEST - Method:keyboard-interactive,Username:mailbox" type="SSH FTP" layer="authentication" mailbox="mailbox@host"></Request>
            if (mailbox==null) {
                String attribute = event.getAttribute("mailbox");
                if (attribute!=null && !attribute.isEmpty()) {
                    String[] pair = attribute.split("@", 2);
                    mailbox = pair[0];
                    host    = pair.length>1 ? pair[1] : null;
                }
            }
            if (type==null) {
                String attribute = event.getAttribute("type");
                if (attribute!=null && !attribute.isEmpty()) {
                    type = attribute;
                }
            }
        }
        /**
         * Parses a &lt;Response&gt; event looking for the identifying
         * protocol ({@code type}) dependent signature in the {@code host}
         * attribute that identifies a successful login.
         * @param event the event to parse
         */
        public void parseResponse(Element event) {
            // <Response host="SSH_MSG_USERAUTH_SUCCESS - User mailbox logged in on: Socket[addr=/192.168.50.1,port=55756,localport=10022]" layer="authentication"></Response>
            // <Response host="230 User mailbox logged in on port: 44350 from: 127.0.0.1. There is 1 current login. SessionID=4742"></Response>
            if (!login && type!=null) {
                String hostattr = event.getAttribute("host");
                if (hostattr!=null && !hostattr.isEmpty()) {
                    Pattern login_response = LOGIN_RESPONSE.get(type);
                    if (login_response!=null) {
                        Matcher matcher = login_response.matcher(hostattr);
                        if (matcher.matches()) {
                            if (mailbox==null) {
                                String useralias = matcher.group(1);
                                String hostalias = find_host(type, useralias);
                                if (hostalias!=null) {
                                    mailbox = useralias;
                                    host    = hostalias;
                                }
                            }
                            if (mailbox!=null) {
                                login = true;
                                trigger(Configuration.Event.login, "host", host, "mailbox", mailbox);
                            }
                        }
                    }
                }
            }
        }
        /**
         * Parses a &lt;Response&gt; event looking for the identifying
         * protocol ({@code type}) dependent signature in the {@code host}
         * attribute that identifies a successful login.
         * @param event the event to parse
         */
        public void parseFile(Element event) {
            //<File transferID="SSH FTP-20150407_192103730-S" destination="/path/file.ext" fileSize="2502"
            //      fileTimeStamp="2015/04/07 19:21:03" direction="Host-&gt;Local" host="sftp users" mailbox="cs"
            //      username="cs" channel="0"></File>
            long start = System.currentTimeMillis();
            trigger(Configuration.Event.file, "host",    event.getAttribute("host"),
                                              "mailbox", event.getAttribute("mailbox"),
                                              "file",    event.getAttribute("destination"),
                                              "user",    event.getAttribute("username"),
                                              //TODO: Temporary location - refactor this.
                                              "startTime",""+ start);
        }
       /**
         * Triggers the &lt;logout&gt; action for the {@code mailbox} if
         * this session was ever logged in.
         */
        public void disconnect() {
            if (login) {
                trigger(Configuration.Event.logout, "host", host, "mailbox", mailbox,"sessionid",id);
            }
        }
        /**
         * Triggers an action for the session's {@code mailbox}, if
         * the mailbox has been set.
         * @param action the action to trigger
         */
        @SuppressWarnings("unused")
        public void trigger(String action) {
            if (mailbox!=null && host!=null) {
                String[] action_path  = new String[] {host, mailbox, action};
                try {
                    lexicom.startRun(ILexiCom.ACTION, action_path, null, false);
                } catch (Exception ignore) {}
                }
        }
        /**
         * Triggers the event on the session's {@code mailbox}, if the
         * mailbox has been set.  The event (and the host and mailbox) are
         * matched against the rule set, and all matching actions are run in
         * sequence.  Replacement tokens are processed from {@code map}, a
         * sequence of string pairs.
         * @param event
         * @param map
         */
        public void trigger(Configuration.Event event, String...map) {
            Logger.debug("trigger(" + event + ") on " + this.toString());
            if (mailbox==null || host==null) return;
            for (Configuration.Rule rule : Configuration.get(event, host, mailbox)) {
                String[] action_path;
                String[] rule_path   = rule.action.split("/");
                if (rule_path.length==1) {
                    action_path = new String[] {host, mailbox, rule.action};
                } else if (rule_path.length==3) {
                    action_path = rule_path;
                } else {
                    throw new IllegalArgumentException("action or host/mailbox/action expected: "+rule.action);
                }
                try {
                    String[] actionAliasPath = null;
                    if (Configuration.getConfiguration().getClone()) {
                        actionAliasPath = new String[]{action_path[0],
                                action_path[1],
                                lexicom.clone(ILexiCom.ACTION, action_path, true)};
                    }
                    else {
                        actionAliasPath = new String[]{action_path[0],
                                action_path[1],
                                action_path[2]};
                    }
                    final IActionController actionController = lexicom.getActionController(actionAliasPath);
                    final String[] commands = actionController.getBaseCommands();
                    for (int i=0; i<commands.length; i++) {
                        for (int m=0; m<map.length-1; m+=2) {
                            commands[i] = commands[i].replaceAll("%"+map[m]+"%", map[m+1]);
                        }
                    }
                    onTrigger(new TriggerContext(actionAliasPath, commands, event.name(), map,id));
                } catch (Exception e) {
                    Logger.debug(e.toString());
                }
            }
        }
        /**
         * Formats the session as a {@code String} as follows:<br/>
         * &nbsp;&nbsp;<i>type</i>[id]<i>=mailbox</i><br/>
         * <i>type</i> and <i>=mailbox</i> are omitted if values are not set.
         */
        public String toString() {
            StringBuilder s = new StringBuilder();
            if (type!=null) {
                s.append(type);
            }
            s.append('[').append(id).append(']');
            if (mailbox!=null) {
                s.append('=').append(mailbox);
            }
            if (host!=null) {
                s.append('@').append(host);
            }
            return s.toString();
        }
    }


    /**
     * Returns the existing {@code Session} for {@id}, creating and registering
     * a new one if one does not yet exist.
     *
     * @param id the CommandID identifying the {@code Session}
     * @return a new or existing {@code Session}
     */
    private Session getSession(String id) {
        Session session = sessions.get(id);
        if (session == null) {
            session = new Session(id);
            sessions.put(id, session);
        }
        return session;
    }

    /**
     * Discards (and returns) the {@code Session} for {@code id}, if one
     * exists.  Should be called for &lt;Disconnect&gt; events.
     *
     * @param id the CommandID identifying the {@code Session}
     * @return the {@code Session}, or {@code null}
     */
    private Session removeSession(String id) {
        return sessions.remove(id);
    }



    /**
     * Processes a {@code LexiComLogEvent} passed from VersaLex.
     */
    @Override
    public void log(LexiComLogEvent e) {
        Element event = e.getEvent();
        String name = event.getNodeName();
        String command = e.getCommandID();
        Session session = getSession(command);
        switch (name) {
            case "Disconnect":
                session.disconnect();
                removeSession(command);
                break;
            case "Request":
                session.parseRequest(event);
                break;
            case "Response":
                session.parseResponse(event);
                break;
            case "File":
                session.parseFile(event);
                break;
            default:
                break;
        }
    }

    protected abstract void onTrigger(TriggerContext context) throws Exception;


    /***
     * A container for passing in environment parameters to execute commands.
      */
    protected static class TriggerContext{
        private final String[] actionAliasPath;
        private final String eventName;
        private final String[] commands;
        private final String[] parameters;
        private final String sessionid;

        public TriggerContext(String[] actionAliasPath,  String[] commands,String eventName, String[] parameters,String sid) {
            this.actionAliasPath = actionAliasPath;
            this.eventName = eventName;
            this.commands = commands;
            this.parameters = parameters;
            sessionid = sid;
        }

        public String[] getActionAliasPath() {
            return actionAliasPath;
        }

        public String[] getCommands() {
            return commands;
        }

        public String getEventName() {
            return eventName;
        }

        public String[] getParameters() {
            return parameters;
        }

        public String getSessionid() {
            return sessionid;
        }
    }

}
