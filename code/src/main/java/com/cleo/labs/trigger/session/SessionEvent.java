package com.cleo.labs.trigger.session;

import com.cleo.labs.trigger.persist.akka.events.IdentifiableLexEvent;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sbelur on 23/09/15.
 */
public final class SessionEvent {

    private final Set<IdentifiableLexEvent> batch = new LinkedHashSet<>();

    private AtomicBoolean frozen = new AtomicBoolean(false);

    public boolean addToBatch(IdentifiableLexEvent event){
        if(!frozen.get())
            return batch.add(event);
        return false;
    }

    public Set<IdentifiableLexEvent>  get(){
        frozen.set(true);
        return batch;
    }

}
