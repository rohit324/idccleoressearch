package com.cleo.labs.trigger.cluster;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.cluster.Cluster;
import akka.cluster.sharding.ClusterSharding;
import com.cleo.labs.trigger.AbstractEventTrigger;
import com.cleo.labs.trigger.Configuration;
import com.cleo.labs.trigger.cluster.support.ClusterSystem;
import com.cleo.labs.trigger.persist.akka.messages.ActionCommand;

/**
 * Created by sbelur on 20/09/15.
 */
public class ClusterBasedPersistenceBackedEventTrigger extends AbstractEventTrigger {

    private volatile ActorSystem actorSystem;
    private Object LOCK = new Object();

    public ClusterBasedPersistenceBackedEventTrigger() {
        super();
    }


    @Override
    protected void onTrigger(TriggerContext context) throws Exception {
        switch (Configuration.getConfiguration().getPersistenceMechanism()) {
            case "akka": {

                //TODO : is there a better way to load it eagerly other than class load time.
                lazyLoadActorSystem();

                boolean clusteredPersistorSetup = ClusterSystem.getInstance().getActorSystem().settings().config().getBoolean("clusteredPersistorSetup");
                String nodeHost = Cluster.get(ClusterSystem.getInstance().getActorSystem()).selfAddress().toString();


                if (!clusteredPersistorSetup) {
                    ActorRef eventPersistentActor = ClusterSystem.getInstance().getClusterProxy();
                    System.out.println("eventPersistentActorRef " + eventPersistentActor);
                    eventPersistentActor.tell(new ActionCommand(context.getCommands(), context.getActionAliasPath(), context.getEventName(), context.getParameters(), context.getSessionid(),nodeHost), ActorRef.noSender());
                } else {
                    ActorRef eventPersistentShardActor = ((ClusterSharding) ClusterSharding.apply(actorSystem)).shardRegion("LexEventPersistor");
                    System.out.println("eventPersistentShardActor " + eventPersistentShardActor);
                    eventPersistentShardActor.tell(new ActionCommand(context.getCommands(), context.getActionAliasPath(), context.getEventName(), context.getParameters(), context.getSessionid(),nodeHost), ActorRef.noSender());
                }

            }
            // others on demand here - as of now its akka only
        }
    }

    private void lazyLoadActorSystem() {
        if (actorSystem == null) {
            synchronized (LOCK) {
                if (actorSystem == null) {
                    try {
                        ClusterSystem clusterSystem = ClusterSystem.getInstance();
                        clusterSystem.initalize();
                        actorSystem = clusterSystem.getActorSystem();
                        System.out.println("ActorSystem created " + actorSystem);
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
        }
    }

}
