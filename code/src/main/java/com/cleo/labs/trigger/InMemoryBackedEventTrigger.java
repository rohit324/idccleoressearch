package com.cleo.labs.trigger;

import com.cleo.lexicom.external.IActionController;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class InMemoryBackedEventTrigger extends AbstractEventTrigger {

    private static ThreadPoolExecutor pool = new ThreadPoolExecutor(
            Configuration.DEFAULT_THREADS, Configuration.DEFAULT_THREADS,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>());


    public InMemoryBackedEventTrigger() {

    }


    @Override
    protected void onTrigger(final TriggerContext context) throws Exception{

        int threads = Configuration.getConfiguration().getThreads();
        final String[] actionAliasPath = context.getActionAliasPath();
        final IActionController actionController = lexicom.getActionController(actionAliasPath);
        if (pool.getCorePoolSize() != threads) {
            Logger.debug("changing trigger thread pool size from " + pool.getCorePoolSize() + " to " + threads);
            pool.setCorePoolSize(threads);
            pool.setMaximumPoolSize(threads);
        }
        pool.execute(new Runnable() {
            @Override
            public void run() {
                String[] commands = context.getCommands();
                try {
                    ExecutionUtil.execute(commands, actionAliasPath, lexicom);
                }
                catch (Exception exp){
                    Logger.debug(exp);
                }
            }
        });
    }
}