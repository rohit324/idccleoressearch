package com.cleo.labs.trigger.persist.akka.messages;

import org.xbill.DNS.Serial;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sbelur on 23/10/15.
 */
public final class MapInput implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Map<String,Serializable> map;


    public MapInput(Map<String, Serializable> map) {
        this.map = map;
    }


    public Serializable getKey(String id){
        return map.get(id);
    }
}
