package com.cleo.labs.trigger.cluster.support;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.sharding.ClusterSharding;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.cleo.labs.trigger.persist.akka.messages.Discard;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

/**
 * Created by sbelur on 11/10/15.
 */
public class PingActor extends UntypedActor {


    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    @Override
    public void preStart() throws Exception {
        super.preStart();
        log.info("Starting ping actor");
        getContext().system().scheduler().schedule(FiniteDuration.apply(1, TimeUnit.MINUTES), FiniteDuration.apply(1, TimeUnit.SECONDS), new Runnable() {
            @Override
            public void run() {
                getSelf().tell("ping", ActorRef.noSender());
            }
        }, getContext().system().dispatcher());
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if("ping".equals(message.toString())) {
            //log.info("Pinging all persistors");
            int  totalInstances = getContext().system().settings().config().getInt("noOfTopics");
            ActorRef eventPersistentShardActor = ((ClusterSharding) ClusterSharding.apply(ClusterSystem.getInstance().getActorSystem())).shardRegion("LexEventPersistor");
            //System.out.println("eventPersistentShardActor in ping " + eventPersistentShardActor);
            for(int ctr=0;ctr<totalInstances;ctr++) {
                eventPersistentShardActor.tell(new Discard(ctr), ActorRef.noSender());
            }
        }
        else {
            unhandled(message);
        }
    }
}
