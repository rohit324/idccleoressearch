package com.cleo.labs.trigger.persist.akka.events;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sbelur on 20/09/15.
 */
public final class IdentifiableLexEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private final Long eventDeliveryId;
    private final LexEvent lexEvent;
    private long lat = -1;


    public IdentifiableLexEvent(Long eventDeliveryId, LexEvent lexEvent) {
        this.eventDeliveryId = eventDeliveryId;
        this.lexEvent = lexEvent;
    }

    public Long getEventDeliveryId() {
        return eventDeliveryId;
    }

    public LexEvent getLexEvent() {
        return lexEvent;
    }


    public long getLatency() {

        if (lat == -1) {
            List list = Arrays.asList(lexEvent.getParameters());
            int index = list.indexOf("startTime");
            if (index != -1) {
                long s = Long.parseLong(lexEvent.getParameters()[index + 1]);
                long end = System.currentTimeMillis();
                long l = end - s;
                lat = l;
            }
        }
        return lat;

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdentifiableLexEvent that = (IdentifiableLexEvent) o;

        return !(eventDeliveryId != null ? !eventDeliveryId.equals(that.eventDeliveryId) : that.eventDeliveryId != null);

    }

    @Override
    public int hashCode() {
        return eventDeliveryId != null ? eventDeliveryId.hashCode() : 0;
    }


    @Override
    public String toString() {
        return super.toString() + " IdentifiableLexEvent{" +
                "eventDeliveryId=" + eventDeliveryId +
                ", lexEvent=" + lexEvent +
                '}';
    }
}
