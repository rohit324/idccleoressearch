package com.cleo.apiAutomation.Trigger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.cleo.lexicom.Sync.Versalex;
import com.cleo.lexicom.external.ISchedule.TRIGGER_TYPE;

import junit.framework.TestCase;

/**
 * 
 * @author manjunathm
 *
 */
public class TriggerTests extends TestCase{
	
	TriggerBase tb = null ;
	FTPFileUpload fu = null ;
	SFTPFileUpload su = null ;
	
//	@Before
	public void setup() throws Exception{
		tb.createHost("Generic SSH FTP", "SSHFTP", false);
	}
	
//	@After
	public void teardown() throws Exception{
		tb.removeItemTrigger("FTP\\ftp_plain\\send",true);
	}
	
	@Test
	public void testSftpTrigger() throws Exception{
		tb.createTriggerItemAndVerify("SSHFTP\\sshftp_plain\\send", TRIGGER_TYPE.NewFileCreated);
		su.sshFileUpload();
		TriggerBase.checkIfFileExists("C:\\Users\\manjunathm\\Desktop\\mallorca\\");
		
	}
	
	@Test
	public void testFtpTrigger() throws Exception{
		tb.removeItemTrigger("FTP\\ftp_plain\\send",true);
		tb.createTriggerItemAndVerify("FTP\\ftp_plain\\send", TRIGGER_TYPE.NewFileCreated);
		tb.createTriggerItemAndVerify("SSHFTP\\sshftp_plain\\send", TRIGGER_TYPE.NewFileCreated);
		tb.createTriggerItemAndVerify("AS2\\plain\\send", TRIGGER_TYPE.NewFileCreated);
		tb.createTriggerItemAndVerify("Generic HSP\\myMailbox\\send", TRIGGER_TYPE.NewFileCreated);
	//	FTPFileUpload.ftpFileUpload();
	//	TriggerBase.checkIfFileExists("C:\\Users\\manjunathm\\Desktop\\mallorca\\");		
	}
	
	

}
