/**
 * 
 */
package persistance.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.exceptions.AlreadyExistsException;

/**
 * @author ragarwal
 *
 */
public class CassandraPersistor {
	
	private static final String CREATE_KEYSPACE = "CREATE KEYSPACE POC WITH REPLICATION = {'class' : 'SimpleStrategy','replication_factor' : 3};";
	private static final String USER_KEYSPACE = "USE POC";
	private static final String CREATE_TABLE = "CREATE TABLE pocTable1 ( id varchar, name varchar, date varchar);";

	private static Cluster cluster;
	private static Session session; 

	static {
		cluster = Cluster.builder().addContactPoint("localhost").build();
		session = cluster.connect();
	}

	public static void main(String[] args) {
		createKeySpace();
		createPOCTable();
		insertSomeValues();
		cleanUp();
	}

	private static void insertSomeValues() {
		int counter = 1;
		for(int i=0;i<10;i++) {
			
			String INSERT_QUERY = "INSERT INTO pocTable (id, name, email) VALUES ('id"+counter+"','poc"+counter+"','poc"+counter+"@gmail.com');";
			session.execute(INSERT_QUERY);
			counter++;
		}
	}


	private static void createPOCTable() {
		try {
			session.execute(CREATE_TABLE);
		}catch(AlreadyExistsException ex) {
			System.out.println("Ignoring the already existing table creation error." + ex.getMessage());
		}
	}

	private static void createKeySpace() {
		try {
			session.execute(CREATE_KEYSPACE);
		}catch(AlreadyExistsException ex) {
			System.out.println("Ignoring the already existing namespace creation error." + ex.getMessage());
		}
		session.execute(USER_KEYSPACE);
	}

	private static void cleanUp() {
		session.close();
		cluster.close();
	}

}
