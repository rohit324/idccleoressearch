/**
 * 
 */
package simplepersistance;

import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.w3c.dom.Element;

import com.cleo.lexicom.beans.LexFile;
import com.cleo.lexicom.streams.LexFileInputStream;
import com.cleo.util.logger.XMLLogElement;

/**
 * @author ragarwal
 *
 */
public class TwoFileRecoverer {
	
	private  synchronized void recoverEvents() throws Exception{
		if(!new LexFile(TwoFilePersistor.eventFileName).exists() ||  !new LexFile(TwoFilePersistor.doneFileName).exists()) {
			System.out.println("nothing to recover.");
			return;
		}
		HashMap<String,Element> startedEvents = getEventsFromFile(new LexFile(TwoFilePersistor.eventFileName));
		HashMap<String,Element> doneEvents = getEventsFromFile(new LexFile(TwoFilePersistor.doneFileName));
		Set<Entry<String, Element>> entrySet = doneEvents.entrySet();
		for (Entry<String, Element> entry : entrySet) {
			startedEvents.remove(entry.getKey());
		}
		if(startedEvents.size() ==0) {
			System.out.println("no events to recover.");
			return;
		}
		System.out.println("recovering the following events.");
		System.out.println(startedEvents);
		
		Set<Entry<String, Element>> eventSet = startedEvents.entrySet();
		for (Entry<String, Element> entry : eventSet) {
			 if(ExecutionUtil.eventHandlingType.equals(ExecutionUtil.ExecutionType.ASYNC)) {
				 ExecutionUtil.workerPool.submit(new EventExecutorTask(entry.getValue()));
			 } else {
				 ExecutionUtil.processEvent(entry.getValue());
			 }
		}
	}
	
	public static HashMap<String,Element> getEventsFromFile(LexFile file) throws Exception {
		HashMap<String,Element> map = new HashMap<String,Element>();
		long length = file.length();
		byte[] data = new byte[new Long(length).intValue()];
		LexFileInputStream inStream = new LexFileInputStream(file, false);
		inStream.read(data);
		String eventsString = new String(data);
		String[] events = eventsString.split(TwoFilePersistor.eventSep);
		for (String event : events) {
			String[] eventData = event.split("\n");
			if(eventData.length==0) continue;
			XMLLogElement xmlLogElement = new XMLLogElement("file","dateTime",new String[]{},new String[]{},"","yellow");
			String uuid="";
			for (String dataNode: eventData) {
				String[] valueItems = dataNode.split(TwoFilePersistor.keyValueSep);
				if(valueItems.length!=2) continue;
				if(valueItems[0].trim().equalsIgnoreCase("uuid")) {
					uuid = valueItems[1];
				} else {
				xmlLogElement.attribute(valueItems[0], valueItems[1]);
				}
			}
			map.put(uuid,ExecutionUtil.getElement(xmlLogElement));
		}
		inStream.close();
		return map;
	}

}
