/**
 * 
 */
package simplepersistance;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import research.Tester;
import research.Tester.Mode;
import research.triggers.FileCreatedTrigger;
import research.triggers.Trigger;
import research.triggers.TriggerUtils;
import simplepersistance.TwoFilePersistor.PERSISTANCE_TYPE;

/**
 * @author ragarwal
 *
 */
public class JsonFilePersistor implements IEventPersistor{
	
	public static String storeDir = "./jsonfile";
	// 10 MB default file size before roll over
	public static int DEFAULT_FILE_SIZE = 10000000;
	public static Object storeKeepersManagementLock = new Object();
	
	public String storeFileName = "triggers.json";
	public String currentStoreFileName = storeFileName;
	private StoreKeepersForJson currentStoreKeeper;
	public static ArrayList<StoreKeepersForJson> stores = new ArrayList<StoreKeepersForJson>(); 
	private static AtomicInteger storeCounter = new AtomicInteger();
	private static Map<String, StoreKeepersForJson> triggerMap = new HashMap<String, StoreKeepersForJson>();
	private static JsonFilePersistor instance = new JsonFilePersistor(); 

	
	{
		new File(storeDir).mkdirs();
		createNewStore();
		triggerMap = Collections.synchronizedMap(triggerMap);
	}


	private void createNewStore() {
		try {
			currentStoreKeeper = new StoreKeepersForJson(new File(storeDir,currentStoreFileName));
			synchronized (storeKeepersManagementLock) {
				stores.add(currentStoreKeeper);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private void cleanUpOldStores() throws IOException {
		ArrayList<StoreKeepersForJson> cleanedStores = new ArrayList<StoreKeepersForJson>();
		
		if(stores.size()<2 ) {
			// lets not even try to clean anything unless we have 2 stores.
			System.out.println("no stores to clean");
			return;
		}
			
			
		for (StoreKeepersForJson storeKeeper : stores) {
			if(triggerMap.containsValue(storeKeeper)) continue;
			if(storeKeeper.cleanUp()) {
			cleanedStores.add(storeKeeper);
			}
		}

		if(cleanedStores.size()>0) {
			synchronized (storeKeepersManagementLock) {
				for (StoreKeepersForJson cleanStore : cleanedStores) {
					stores.remove(cleanStore);
				}
			}
		}
	}
	
	
	public static JsonFilePersistor getInstance() {
		return instance;
	}
	
	private synchronized void writeTriggerData(Trigger trigger) throws IOException  {
		if(isTimeForRollOver()) {
			currentStoreFileName = storeFileName+ storeCounter.incrementAndGet();
			createNewStore();
			currentStoreKeeper.storeStartTrigger(trigger);
			cleanUpOldStores();
		}  else {
			currentStoreKeeper.storeStartTrigger(trigger);
		}
		
		triggerMap.put(trigger.getUuid(), currentStoreKeeper);
	}
	
	public boolean isTimeForRollOver() {
		File file = new File(storeDir,currentStoreFileName);
		return file.length()>=DEFAULT_FILE_SIZE;
	}
	
	private synchronized void writeTriggerDoneData(String uuid) throws IOException {
		StoreKeepersForJson storeKeepers = triggerMap.get(uuid);
		storeKeepers.storeEndTrigger(uuid);
		triggerMap.remove(uuid);
	}
	
	public static void main(String[] args) throws Exception
	{
		performTestSingularPersistJsonFormat();
//		testSync();
	}
	
	/**
	 * @return the currentStoreKeeper
	 */
	public StoreKeepersForJson getCurrentStoreKeeper() {
		return currentStoreKeeper;
	}

	private static void testSync() throws Exception {
		for(int i=0;i<10;i++) {
			FileCreatedTrigger trigger = TriggerUtils.createSampleTrigger();
			instance.persistMessage(trigger, trigger.getUuid());
			instance.deleteMessage(trigger.getUuid());
		}
	}
	
	public static HashMap<String, String> performTestSingularPersistJsonFormat() throws Exception{
		System.out.println("***collecting stats for jdk threads with Singlular Persistance in a single File in JSON format***.");	
		System.setProperty(Tester.PROP_NAME_PERSIST, "true");
		System.setProperty(Tester.PROP_NAME_PERSIST_STRATEGY, PERSISTANCE_TYPE.JSON.toString());
		String executionMode = Mode.jdk_threads.toString();
		System.setProperty(Tester.EXECUTION_MODE, executionMode);
		Tester.startMonitoring();
		Tester.collectStatsForExecutionMode(Mode.jdk_threads_persistence_single_file_json.toString(), Tester.initialCount);
		HashMap<String, String> metricMap = Tester.stopMonitoring();
		return metricMap;
		
	}

	/* (non-Javadoc)
	 * @see simplepersistance.IEventPersistor#persistMessage(java.lang.Object, java.lang.String)
	 */
	@Override
	public void persistMessage(Object event, String uuid) throws Exception {
		writeTriggerData((Trigger)event);
	}

	/* (non-Javadoc)
	 * @see simplepersistance.IEventPersistor#deleteMessage(java.lang.String)
	 */
	@Override
	public void deleteMessage(String uuid) throws Exception {
		writeTriggerDoneData(uuid);
	}
	
}


class StoreKeepersForJson {
	
	private File storeFile;
	private OutputStream writeStream;
	private AtomicInteger triggerCounter = new AtomicInteger();
	ObjectMapper mapper = null;
	
	public StoreKeepersForJson(File storeFile) throws FileNotFoundException {
		super();
		this.storeFile = storeFile;
		writeStream = new FileOutputStream(storeFile);
		mapper = new ObjectMapper();

	}

	/**
	 * @return the storeFile
	 */
	public File getStoreFile() {
		return storeFile;
	}

	/**
	 * @param storeFile the storeFile to set
	 */
	public void setStoreFile(File storeFile) {
		this.storeFile = storeFile;
	}
	
	public synchronized void storeStartTrigger(Trigger trigger) throws IOException {
		// Convert object to JSON string
		String jsonInString = mapper.writeValueAsString(trigger);
		writeStream.write(("EventStart:::"+jsonInString+"\n").getBytes());
		triggerCounter.incrementAndGet();
	}
	
	public synchronized void storeEndTrigger(String uuid) throws IOException {
		writeStream.write(("EventEnd:::"+uuid+"\n").getBytes());
		triggerCounter.decrementAndGet();
	}
	
	public boolean cleanUp() throws IOException {
//		System.out.println("cleaning up store " + storeFile.getName());
		if(triggerCounter.get()==0) {
			System.out.println("cleaning up store " + storeFile.getName());
			writeStream.flush();
			writeStream.close();
			return storeFile.delete();
		} else {
//			System.out.println("trigger count for file " + storeFile.getName() +"=="+ triggerCounter.get());
		}
		return false;
	}
	
	public boolean allEventsDone() {
		return !storeFile.exists();
	}
	
}
