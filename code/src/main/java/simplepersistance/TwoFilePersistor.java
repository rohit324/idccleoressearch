/**
 * 
 */
package simplepersistance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import research.JDKThread;
import research.Tester;
import research.Tester.Mode;
import research.triggers.FileCreatedTrigger;
import research.triggers.Trigger;
import research.triggers.TriggerUtils;

import com.cleo.lexicom.beans.LexFile;
import com.cleo.lexicom.streams.LexFileOutputStream;
import com.cleo.util.logger.XMLLogElement;



/**
 * @author ragarwal
 *
 */
public class TwoFilePersistor implements IEventPersistor {

	public static String storeDir = "./twoFiles";

	public static int DEFAULT_FILE_SIZE = 10000000;
	public static Object storeKeepersManagementLock = new Object();
	public static String eventFileName = "event.data";
	public String currentEventFileName = eventFileName;

	public static String doneFileName = "completed.data";
	public String currentDoneFileName = doneFileName;
	private static AtomicInteger storeCounter = new AtomicInteger();


	static String eventSep = "-------------";
	static String keyValueSep = "==";
	private static Map<String, TwoFilesStoreManager> triggerMap = new HashMap<String, TwoFilesStoreManager>();
	TwoFilesStoreManager currentStoreKeeper;
	public static ArrayList<TwoFilesStoreManager> stores = new ArrayList<TwoFilesStoreManager>();

	{
		new File(storeDir).mkdirs();
		createNewStore();
		triggerMap = Collections.synchronizedMap(triggerMap);
	}

	private void createNewStore() {
		try {
			currentStoreKeeper = new TwoFilesStoreManager(new File(storeDir,currentEventFileName), new File(storeDir,currentDoneFileName));
			synchronized (storeKeepersManagementLock) {
				stores.add(currentStoreKeeper);
			}
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	

	/**
	 * @return the currentStoreKeeper
	 */
	public TwoFilesStoreManager getCurrentStoreKeeper() {
		return currentStoreKeeper;
	}



	public static enum PERSISTANCE_TYPE{
		SINGLUAR,
		CONCURRENT,
		SINGLUAR_ONE_FILE,
		JSON
	}


	public static void main(String[] args) throws Exception {
		//		testUsingXMLElements();
//		performTestSingularPersist();
//		JDKThread.getWorkerPool().shutdown();

		testSyncFlow();
	}
	
	private static void testSyncFlow() throws Exception {
		
		FileCreatedTrigger trigger = TriggerUtils.createSampleTrigger();
		IEventPersistor twoInstance = PersistorFactory.getInstance(PERSISTANCE_TYPE.SINGLUAR);
		
		IEventPersistor instance = PersistorFactory.getInstance(PERSISTANCE_TYPE.SINGLUAR_ONE_FILE);
		
		long startTime = System.currentTimeMillis();
		for(int i=0;i<50000;i++) {
			SingleFilePersistor persistor = (SingleFilePersistor)instance;
			persistor.persistMessage(trigger, trigger.getUuid());
//			persistor.getCurrentStoreKeeper().storeStartTrigger(trigger);
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("time taken = " + (endTime-startTime));
		
		startTime = System.currentTimeMillis();
		for(int i=0;i<50000;i++) {
			TwoFilePersistor Twopersistor = (TwoFilePersistor)twoInstance;
			Twopersistor.persistMessage(trigger, trigger.getUuid());
//			Twopersistor.getCurrentStoreKeeper().storeStartTrigger(trigger);
		}
		
		endTime = System.currentTimeMillis();
		System.out.println("time taken = " + (endTime-startTime));
	}

	public void persistMessage(Object event, String uuid) throws Exception {
		if(isTimeForRollOver()) {
			synchronized (this) {
				if(isTimeForRollOver()) {
				int counter = storeCounter.incrementAndGet();
				currentEventFileName = eventFileName+ counter;
				currentDoneFileName = doneFileName+ counter;
				createNewStore();
				currentStoreKeeper.storeStartTrigger((Trigger)event);
				cleanUpOldStores();
				} else {
					currentStoreKeeper.storeStartTrigger((Trigger)event);
				}
			}
			
		}  else {
			currentStoreKeeper.storeStartTrigger((Trigger)event);
		}

		triggerMap.put(((Trigger)event).getUuid(), currentStoreKeeper);
	}


	private void cleanUpOldStores() throws IOException {
		ArrayList<TwoFilesStoreManager> cleanedStores = new ArrayList<TwoFilesStoreManager>();

		if(stores.size()<2 ) {
			// lets not even try to clean anything unless we have 2 stores.
			System.out.println("no stores to clean");
			return;
		}


		for (TwoFilesStoreManager storeKeeper : stores) {
			if(triggerMap.containsValue(storeKeeper)) continue;
			if(storeKeeper.cleanUp()) {
				cleanedStores.add(storeKeeper);
			}
		}

		if(cleanedStores.size()>0) {
			synchronized (storeKeepersManagementLock) {
				for (TwoFilesStoreManager cleanStore : cleanedStores) {
					stores.remove(cleanStore);
				}
			}
		}
	}

	public boolean isTimeForRollOver() {
		File file = new File(storeDir,currentEventFileName);
		return file.length()>=DEFAULT_FILE_SIZE;
	}

	public void deleteMessage(String uuid) throws Exception {
		TwoFilesStoreManager storeKeepers = triggerMap.get(uuid);
		storeKeepers.storeEndTrigger(uuid);
		triggerMap.remove(uuid);
	}

	public static HashMap<String, String> performTestSingularPersist() throws Exception{
		System.out.println("***collecting stats for jdk threads with Singlular Persistance***.");	
		System.setProperty(Tester.PROP_NAME_PERSIST, "true");
		System.setProperty(Tester.PROP_NAME_PERSIST_STRATEGY, PERSISTANCE_TYPE.SINGLUAR.toString());
		String executionMode = Mode.jdk_threads.toString();
		System.setProperty(Tester.EXECUTION_MODE, executionMode);
		Tester.startMonitoring();
		Tester.collectStatsForExecutionMode(Mode.jdk_threads_persistance.toString(), Tester.initialCount);
		HashMap<String, String> metricMap = Tester.stopMonitoring();
		return metricMap;

	}

}


class TwoFilesStoreManager {

	private File eventFile;
	private File doneFile;
	private OutputStream eventWriteStream;
	private OutputStream doneWriteStream;
	private AtomicInteger triggerCounter = new AtomicInteger();
	private Object eventLock = new Object();
	private Object doneLock = new Object();

	public TwoFilesStoreManager(File eventFile, File doneFile) throws FileNotFoundException {
		super();
		this.eventFile = eventFile;
		this.doneFile = doneFile;
		eventWriteStream = new FileOutputStream(eventFile);
		doneWriteStream = new FileOutputStream(doneFile);
	}



	public void storeStartTrigger(Trigger trigger) throws IOException {
		synchronized (eventLock) {
			eventWriteStream.write(trigger.toString().getBytes());
//			eventWriteStream.write(("\n"+TwoFilePersistor.eventSep+"\n").getBytes());
		}
		triggerCounter.incrementAndGet();
	}

	public void storeEndTrigger(String uuid) throws IOException {
		synchronized (doneLock) {
			doneWriteStream.write(uuid.getBytes());
//			doneWriteStream.write(("\n"+TwoFilePersistor.eventSep+"\n").getBytes());
		}
		triggerCounter.decrementAndGet();
	}

	public boolean cleanUp() throws IOException {
		if(triggerCounter.get()==0) {
			System.out.println("cleaning up store " + eventFile.getName());
			eventWriteStream.flush();
			eventWriteStream.close();
			doneWriteStream.flush();
			doneWriteStream.close();
			return eventFile.delete() && doneFile.delete();
		} else {
//			System.out.println("trigger count for file " + eventFile.getName() +"=="+ triggerCounter.get());
		}
		return false;
	}

	public boolean allEventsDone() {
		return !eventFile.exists();
	}

}
