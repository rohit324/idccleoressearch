/**
 * 
 */
package simplepersistance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import research.Tester;
import research.Tester.Mode;
import research.triggers.FileCreatedTrigger;
import research.triggers.Trigger;
import research.triggers.TriggerUtils;
import simplepersistance.TwoFilePersistor.PERSISTANCE_TYPE;

/**
 * @author ragarwal
 *
 */
public class SingleFilePersistor implements IEventPersistor{
	
	public static String storeDir = "./singleFile";
	// 10 MB default file size before roll over
	public static int DEFAULT_FILE_SIZE = 10000000;
	public static Object storeKeepersManagementLock = new Object();
	
	public String storeFileName = "triggers.data";
	public String currentStoreFileName = storeFileName;
	private StoreKeepers currentStoreKeeper;
	public static ArrayList<StoreKeepers> stores = new ArrayList<StoreKeepers>(); 
	private static AtomicInteger storeCounter = new AtomicInteger();
	private static Map<String, StoreKeepers> triggerMap = new HashMap<String, StoreKeepers>();
	private static SingleFilePersistor instance = new SingleFilePersistor(); 

	
	{
		new File(storeDir).mkdirs();
		createNewStore();
		triggerMap = Collections.synchronizedMap(triggerMap);
	}


	private void createNewStore() {
		try {
			currentStoreKeeper = new StoreKeepers(new File(storeDir,currentStoreFileName));
			synchronized (storeKeepersManagementLock) {
				stores.add(currentStoreKeeper);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private void cleanUpOldStores() throws IOException {
		ArrayList<StoreKeepers> cleanedStores = new ArrayList<StoreKeepers>();
		
		if(stores.size()<2 ) {
			// lets not even try to clean anything unless we have 2 stores.
			System.out.println("no stores to clean");
			return;
		}
			
			
		for (StoreKeepers storeKeeper : stores) {
			if(triggerMap.containsValue(storeKeeper)) continue;
			if(storeKeeper.cleanUp()) {
			cleanedStores.add(storeKeeper);
			}
		}

		if(cleanedStores.size()>0) {
			synchronized (storeKeepersManagementLock) {
				for (StoreKeepers cleanStore : cleanedStores) {
					stores.remove(cleanStore);
				}
			}
		}
	}
	
	
	public static SingleFilePersistor getInstance() {
		return instance;
	}
	
	private synchronized void writeTriggerData(Trigger trigger) throws IOException  {
		if(isTimeForRollOver()) {
			currentStoreFileName = storeFileName+ storeCounter.incrementAndGet();
			createNewStore();
			currentStoreKeeper.storeStartTrigger(trigger);
			cleanUpOldStores();
		}  else {
			currentStoreKeeper.storeStartTrigger(trigger);
		}
		
		triggerMap.put(trigger.getUuid(), currentStoreKeeper);
	}
	
	public boolean isTimeForRollOver() {
		File file = new File(storeDir,currentStoreFileName);
		return file.length()>=DEFAULT_FILE_SIZE;
	}
	
	private synchronized void writeTriggerDoneData(String uuid) throws IOException {
		StoreKeepers storeKeepers = triggerMap.get(uuid);
		storeKeepers.storeEndTrigger(uuid);
		triggerMap.remove(uuid);
	}
	
	public static void main(String[] args) throws Exception
	{
		performTestSingularPersist();
//		testSync();
	}
	
	/**
	 * @return the currentStoreKeeper
	 */
	public StoreKeepers getCurrentStoreKeeper() {
		return currentStoreKeeper;
	}

	private static void testSync() throws Exception {
		for(int i=0;i<10;i++) {
			FileCreatedTrigger trigger = TriggerUtils.createSampleTrigger();
			instance.persistMessage(trigger, trigger.getUuid());
			instance.deleteMessage(trigger.getUuid());
		}
	}
	
	public static HashMap<String, String> performTestSingularPersist() throws Exception{
		System.out.println("***collecting stats for jdk threads with Singlular Persistance in a single File***.");	
		System.setProperty(Tester.PROP_NAME_PERSIST, "true");
		System.setProperty(Tester.PROP_NAME_PERSIST_STRATEGY, PERSISTANCE_TYPE.SINGLUAR_ONE_FILE.toString());
		String executionMode = Mode.jdk_threads.toString();
		System.setProperty(Tester.EXECUTION_MODE, executionMode);
		Tester.startMonitoring();
		Tester.collectStatsForExecutionMode(Mode.jdk_threads_persistance_single_file.toString(), Tester.initialCount);
		HashMap<String, String> metricMap = Tester.stopMonitoring();
		return metricMap;
		
	}

	/* (non-Javadoc)
	 * @see simplepersistance.IEventPersistor#persistMessage(java.lang.Object, java.lang.String)
	 */
	@Override
	public void persistMessage(Object event, String uuid) throws Exception {
		writeTriggerData((Trigger)event);
	}

	/* (non-Javadoc)
	 * @see simplepersistance.IEventPersistor#deleteMessage(java.lang.String)
	 */
	@Override
	public void deleteMessage(String uuid) throws Exception {
		writeTriggerDoneData(uuid);
	}
	
}


class StoreKeepers {
	
	private File storeFile;
	private OutputStream writeStream;
	private AtomicInteger triggerCounter = new AtomicInteger();
	
	public StoreKeepers(File storeFile) throws FileNotFoundException {
		super();
		this.storeFile = storeFile;
		writeStream = new FileOutputStream(storeFile);
	}

	/**
	 * @return the storeFile
	 */
	public File getStoreFile() {
		return storeFile;
	}

	/**
	 * @param storeFile the storeFile to set
	 */
	public void setStoreFile(File storeFile) {
		this.storeFile = storeFile;
	}
	
	public synchronized void storeStartTrigger(Trigger trigger) throws IOException {
		writeStream.write(("EventStart:::"+trigger.toString()+"\n").getBytes());
		triggerCounter.incrementAndGet();
	}
	
	public synchronized void storeEndTrigger(String uuid) throws IOException {
		writeStream.write(("EventEnd:::"+uuid+"\n").getBytes());
		triggerCounter.decrementAndGet();
	}
	
	public boolean cleanUp() throws IOException {
//		System.out.println("cleaning up store " + storeFile.getName());
		if(triggerCounter.get()==0) {
			System.out.println("cleaning up store " + storeFile.getName());
			writeStream.flush();
			writeStream.close();
			return storeFile.delete();
		} else {
//			System.out.println("trigger count for file " + storeFile.getName() +"=="+ triggerCounter.get());
		}
		return false;
	}
	
	public boolean allEventsDone() {
		return !storeFile.exists();
	}
	
}
