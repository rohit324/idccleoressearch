/**
 * 
 */
package simplepersistance;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import research.Events;
import research.Tester;
import research.Tester.Mode;

import com.cleo.lexicom.beans.LexFile;
import com.cleo.lexicom.streams.LexFileOutputStream;
import com.cleo.util.logger.XMLLogElement;

/**
 * @author ragarwal
 *
 */
public class ConcurrentEventPersistor implements IEventPersistor  {

	private ArrayList<Object> writeLockList = new ArrayList<Object>();
	private ArrayList<Object> doneLockList = new ArrayList<Object>();
	public static String storeDir = "./events";


	int availableProcessors = Runtime.getRuntime().availableProcessors();

	private HashMap<Object, OutputStream> writerStreamMap = new HashMap<Object, OutputStream>(availableProcessors*4);
	private HashMap<Object, OutputStream> doneStreamMap = new HashMap<Object, OutputStream>(availableProcessors*4);


	private  volatile int writeLockCounter = 0;
	private  volatile int doneLockCounter = 0;
	
	{
		initAllStreams();
	}

	private void initAllStreams() {
		new LexFile(storeDir).mkdirs();
		// event recovery will be coded later.
		for(int i=0;i<availableProcessors;i++) {
			try {
				Object writeLockObject = new Object();
				Object doneObject = new Object();
				synchronized (writeLockObject) {
					synchronized (doneObject) {
						writeLockList.add(writeLockObject);
						LexFile writeFile = new LexFile(storeDir,"allEvents.data"+i);
						if(writeFile.exists()) {
							writeFile.delete();
						}
						writerStreamMap.put(writeLockObject, new LexFileOutputStream(writeFile,true));
						doneLockList.add(doneObject);
						LexFile doneFileObject = new LexFile(storeDir,"completed.data"+i);
						if(doneFileObject.exists()) {
							doneFileObject.delete();
						}
						LexFileOutputStream doneFile = new LexFileOutputStream(doneFileObject,true);
						doneStreamMap.put(doneObject, doneFile);
					}
				}
			}catch(Exception ex) {
				ex.printStackTrace();
				throw new RuntimeException(ex);
			}
		}
	}

	

	public void writeEventData(String eventBuffer) throws IOException {
		int index = writeLockCounter++%availableProcessors;
		Object writerLock = writeLockList.get(index);
		synchronized (writerLock) {
			OutputStream stream = writerStreamMap.get(writerLock);
			stream.write(eventBuffer.getBytes());
			stream.write(("\n"+TwoFilePersistor.eventSep+"\n").getBytes());
		}
	}

	/**
	 * 
	 */
	public void deleteAllFiles() throws Exception {
		Collection<OutputStream> values = writerStreamMap.values();
		for (OutputStream outputStream : values) {
			outputStream.close();
		}
		
		Collection<OutputStream> values2 = doneStreamMap.values();
		for (OutputStream outputStream : values2) {
			outputStream.close();
		}
		
		LexFile store = new LexFile(storeDir);
		if(store.exists()) {
			LexFile[] files = store.listFiles();
			for (LexFile lexFile : files) {
				lexFile.delete();
			}
		}
		
	}

	public void writeEventDoneData(String eventBuffer) throws IOException {
		int index = doneLockCounter++%availableProcessors;
		Object doneLock = doneLockList.get(index);
		OutputStream doneStream = doneStreamMap.get(doneLock);
		synchronized (doneLock) {
			doneStream.write(eventBuffer.getBytes());
			doneStream.write(("\n"+TwoFilePersistor.eventSep+"\n").getBytes());
		}
	}


	protected void rollOver() throws Exception {
		for (int i=0;i<doneLockList.size();i++) {
			Object writeLock = writeLockList.get(0);
			synchronized (writeLock) {
				Object doneLock = doneLockList.get(0);
				synchronized (doneLock) {
					writerStreamMap.get(writeLock).close();
					LexFile writeFile = new LexFile(storeDir,"allEvents.data"+i);
					writeFile.delete();
					writeFile = new LexFile(storeDir,"allEvents.data"+i);
					writerStreamMap.put(writeLock, new LexFileOutputStream(writeFile,true));
				}
			}
		}
	}
	
	public static HashMap<String, String> performTestConcurrentPersist() throws Exception{
		System.out.println("***collecting stats for jdk threads with Concurrent Persistance***.");	
		System.setProperty(Tester.PROP_NAME_PERSIST, "true");
		System.setProperty(Tester.PROP_NAME_PERSIST_STRATEGY, TwoFilePersistor.PERSISTANCE_TYPE.CONCURRENT.toString());
		String executionMode = Mode.jdk_threads.toString();
		System.setProperty(Tester.EXECUTION_MODE, executionMode);
		Tester.startMonitoring();
		Tester.collectStatsForExecutionMode(Mode.jdk_threads_concurrent_persistance.toString(), Tester.initialCount);
		HashMap<String, String> metricMap = Tester.stopMonitoring();
		return metricMap;
		
	}

	
	public static void main(String[] args) throws Exception {
		IEventPersistor persistor = PersistorFactory.getInstance(TwoFilePersistor.PERSISTANCE_TYPE.CONCURRENT);
		XMLLogElement xmlLogElement= Events.generateEvent();
		for(int i=0;i<100;i++) {
		persistor.persistMessage(xmlLogElement, i+"");
		persistor.deleteMessage(i+"");
		}
	}



	/* (non-Javadoc)
	 * @see simplepersistance.IEventPersistor#persistMessage(java.lang.Object, java.lang.String)
	 */
	@Override
	public void persistMessage(Object event, String uuid) throws Exception {
		writeEventData(event.toString());
		
	}



	/* (non-Javadoc)
	 * @see simplepersistance.IEventPersistor#deleteMessage(java.lang.String)
	 */
	@Override
	public void deleteMessage(String uuid) throws Exception {
		writeEventDoneData(uuid);
		
	}

}
