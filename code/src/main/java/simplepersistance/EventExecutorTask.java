package simplepersistance;

import org.w3c.dom.Element;

/**
 * @author ragarwal
 *
 */
public class EventExecutorTask implements Runnable{

	public Object event;
	
	public EventExecutorTask(Object event) {
		this.event = event;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		if(event instanceof Element) {
			try {
				ExecutionUtil.processEvent((Element)event);
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			} 
		} else {
			throw new RuntimeException("Need to handle other types too.");
		}
		
	}

}
