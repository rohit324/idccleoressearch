/**
 * 
 */
package ftpclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;


/**
 * @author ragarwal
 *
 */
public class TestFTP8 {

  public static int totalCLients = 25;
  public static int filesPerClient = 2;
  static AtomicInteger filesSent = new AtomicInteger();


  private static int clientCounter = 0;

  public static void main(String[] args) throws Exception {
    for(int i=0;i<100;i++) {
      clientCounter = 0;
      ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(totalCLients);
      filesSent = new AtomicInteger();
      sendFile(newFixedThreadPool);
    }

    System.exit(0);
  }

  private static void sendFile(ExecutorService pool) throws Exception, SocketException, IOException, InterruptedException {
    final List<File> fileList = createDataFiles(totalCLients);
    System.out.println("sample test data created successfully." + fileList);
    startDeleteAndTriggerSizeThread();

    List<FTPClient> ftpClientList = ftpConnection(totalCLients);
    long start = System.currentTimeMillis();
    for (FTPClient ftpClient : ftpClientList) {
      pool.submit(new Runnable() {
        public void run() {
          try {
            File file = fileList.get(clientCounter);
            for(int i=0;i<filesPerClient;i++) {
              exchangeFile(file, ftpClient);
            }
            ftpClient.logout();
            ftpClient.disconnect();
          }  catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
          }
        }
      });
      clientCounter++;
    }

    while (filesSent.get() < (totalCLients*filesPerClient)) {
      Thread.sleep(1);
    }

    long endTime = System.currentTimeMillis();
    System.out.println("files sent  = " + filesSent.get());
    System.out.println("Time taken = " + (endTime - start));
    System.out.println("no of files = " + filesSent);
    pool.shutdownNow();
    pool = null;
  }

  private static void printUsage() throws Exception {
    OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
    Method method = operatingSystemMXBean.getClass().getMethod("getSystemCpuLoad", null);
    method.setAccessible(true);
    Method memMethod = operatingSystemMXBean.getClass().getMethod("getFreePhysicalMemorySize", null);
    memMethod.setAccessible(true);
    Object value = method.invoke(operatingSystemMXBean);
    double parseDouble = Double.parseDouble(value.toString());

    Object memValue = memMethod.invoke(operatingSystemMXBean);
    System.out.println("CPU load = " + (new Double(parseDouble*100).intValue()) + " Memory Usage = " + memValue);
  }

  private static void startDeleteAndTriggerSizeThread() {
    new Thread() {
      public void run() {
        while(true) {
          deleteAllFile();
          System.out.println("printing");
          printTriggerSize();

          try {
            printUsage();
            Thread.currentThread().sleep(1000);
          } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      }
    }.start();;
  }

  private static long startTime = System.currentTimeMillis();

  private static void printTriggerSize() {
    File file = new File("/Users/rohit/software/vagrant/test/Har18/events");
    File[] listFiles = file.listFiles();
    for (File file2 : listFiles) {
      if(file2.isDirectory()) continue;
      long length = file2.length();
      int filesTransfered = filesSent.get();
      float totalEvents = length/400;
      float totalTimeTaken = (System.currentTimeMillis()-startTime)/1000;
      System.out.println("Total files transfered = " + filesTransfered + " files transfer per mili second = " + (filesTransfered/totalTimeTaken));

      System.out.println("no of events fired = " + totalEvents + " in time = " + totalTimeTaken +" events fired per miliseconds second = " + (totalEvents/totalTimeTaken));
    }
  }

  private static void deleteAllFile() {
    String dir = "/Users/rohit/software/vagrant/test/Har18/local/root/ftp_plain";
    File file = new File(dir);
    File[] listFiles = file.listFiles();
    for (File file2 : listFiles) {
      file2.delete();
    }
  }

  public static List<File> createDataFiles(int count) throws Exception{
    List<File> fileList = new ArrayList<File>();
    for(int i=0;i<=count;i++) {
      URL resource = TestFTP8.class.getResource(".");
      File file = new File(resource.getFile()+"test.data"+i);
      FileOutputStream stream = new FileOutputStream(file);
      stream.write("simple test data".getBytes());
      stream.close();
      fileList.add(file);
    }
    return fileList;
  }

  private static void exchangeFile(File file, FTPClient ftpClient) {
    InputStream stream = TestFTP8.class.getResourceAsStream(file
        .getName());
    try {
      ftpClient.storeFile(
          file.getName() + System.nanoTime(), stream);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    } finally {
      if (stream != null)
        try {
          stream.close();
        } catch (IOException e) {
          e.printStackTrace();
          throw new RuntimeException(e);
        }
    }
    filesSent.incrementAndGet();
  }

  /*private static void exchangeFile(File file, FTPClient ftpClient) {

	//  System.out.println(file.getAbsolutePath());
			InputStream stream = TestFTP.class.getResourceAsStream(file
					.getName());

			try {
				ftpClient.storeFile(
						UUID.randomUUID().toString(), stream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (stream != null)
					try {
						stream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			filesSent.incrementAndGet();


	}*/

  private static List<FTPClient> ftpConnection(int totalClients)
      throws SocketException, IOException {
    List<FTPClient> ftpClientList = new ArrayList<FTPClient>();
    for (int i = 0; i < totalClients; i++) {

      String server = "10.20.101.73";
      int port = 5021;
      String user = "ftp_plain";
      String pass = "cleo";
      FTPClient ftpClient = new FTPClient();
      ftpClient.connect(server, port);
      int replyCode = ftpClient.getReplyCode();
      if (!FTPReply.isPositiveCompletion(replyCode)) {
        System.out.println("Operation failed. Server reply code: "
            + replyCode);
        throw new RuntimeException(
            "Operation failed. Server reply code: " + replyCode);
      }
      boolean success = ftpClient.login(user, pass);
      if (!success) {
        System.out.println("Could not login to the server");
        throw new RuntimeException("connection to server failed.");
      } else {
        System.out.println("LOGGED IN SERVER");
        ftpClientList.add(ftpClient);
      }

    }

    return ftpClientList;
  }

}
