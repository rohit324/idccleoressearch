/**
 * 
 */
package ftpclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.net.SocketException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;


/**
 * @author ragarwal
 *
 */
public class TestFTPScheduler {

  public static int totalCLients = 100;
  public static int filesPerClient = 50;
  static ExecutorService workerPool = Executors.newFixedThreadPool(totalCLients);
  static AtomicInteger filesSent = new AtomicInteger();


  private static int clientCounter = 0;

  public static void main(String[] args) throws Exception {
    final List<File> fileList = createDataFiles(totalCLients);
    deleteAndPrintAllFile();
    System.out.println("sample test data created successfully." + fileList);
    startDeleteAndTriggerSizeThread();

    long start = sendFiles(fileList);

    while (filesSent.get() < (totalCLients*filesPerClient)) {
      Thread.sleep(10);
    }

    long endTime = System.currentTimeMillis();
    System.out.println("files sent  = " + filesSent.get());
    System.out.println("Time taken = " + (endTime - start));
    System.out.println("no of files = " + filesSent);
    workerPool.shutdownNow();
    System.exit(0);
  }

  private static long sendFiles(final List<File> fileList) throws SocketException, IOException {
    HashMap<String, FTPClient> clientMap = ftpConnection(totalCLients);
    Set<Entry<String, FTPClient>> clientSet = clientMap.entrySet();
    long start = System.currentTimeMillis();
    for (Entry<String, FTPClient> entry : clientSet) {
      workerPool.submit(new Runnable() {
        public void run() {
          try {
            File file = fileList.get(clientCounter);
            for(int i=0;i<filesPerClient;i++) {
              exchangeFile(file, entry);
            }
            entry.getValue().logout();
            entry.getValue().disconnect();
          }  catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
          }
        }
      });
      clientCounter++;
    }
    return start;
  }
  
  private static void printUsage() throws Exception {
    OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
    Method method = operatingSystemMXBean.getClass().getMethod("getSystemCpuLoad", null);
    method.setAccessible(true);
    Method memMethod = operatingSystemMXBean.getClass().getMethod("getFreePhysicalMemorySize", null);
    memMethod.setAccessible(true);
        Object value = method.invoke(operatingSystemMXBean);
        double parseDouble = Double.parseDouble(value.toString());

        Object memValue = memMethod.invoke(operatingSystemMXBean);
        System.out.println("CPU load = " + (new Double(parseDouble*100).intValue()) + " Memory Usage = " + memValue);
}
  
  private static void startDeleteAndTriggerSizeThread() {
    new Thread() {
      public void run() {
        while(true) {
//        deleteAndPrintAllFile();
        try {
          printUsage();
          System.out.println("Files Transfered = " + filesSent.get());
          String dir = "/Users/rohit/test";
          File file = new File(dir);
          File[] listFiles = file.listFiles();
          System.out.println("Number of Actions executed = " + listFiles.length + " time taken = " + (System.currentTimeMillis()-startTime));
          Thread.currentThread().sleep(1000);
        } catch (Exception e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        }
      }
    }.start();;
  }
  
  private static long startTime = System.currentTimeMillis();
  
  
  private static int scheduleLCOPYFilesCounter = 0;
  private static void deleteAndPrintAllFile() {
    String dir = "/Users/rohit/test";
    File file = new File(dir);
    File[] listFiles = file.listFiles();
    float totalTimeTaken = (System.currentTimeMillis()-startTime)/1000;
    scheduleLCOPYFilesCounter = scheduleLCOPYFilesCounter + listFiles.length;
    for (File file2 : listFiles) {
      file2.delete();
    }
  }

  public static List<File> createDataFiles(int count) throws Exception{
    List<File> fileList = new ArrayList<File>();
    for(int i=0;i<=count;i++) {
      URL resource = TestFTPScheduler.class.getResource(".");
      File file = new File(resource.getFile()+"test.data"+i);
      FileOutputStream stream = new FileOutputStream(file);
      stream.write("simple test data".getBytes());
      stream.close();
      fileList.add(file);
    }
    return fileList;
  }

  private static void exchangeFile(File file, Entry<String, FTPClient> entry) {
    InputStream stream = TestFTPScheduler.class.getResourceAsStream(file
        .getName());
    try {
      FTPClient client = entry.getValue();
      client.cwd(entry.getKey());
      client.storeFile(
          file.getName() + System.nanoTime(), stream);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    } finally {
      if (stream != null)
        try {
          stream.close();
        } catch (IOException e) {
          e.printStackTrace();
          throw new RuntimeException(e);
        }
    }
    filesSent.incrementAndGet();
  }

  /*private static void exchangeFile(File file, FTPClient ftpClient) {

	//  System.out.println(file.getAbsolutePath());
			InputStream stream = TestFTP.class.getResourceAsStream(file
					.getName());

			try {
				ftpClient.storeFile(
						UUID.randomUUID().toString(), stream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (stream != null)
					try {
						stream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			filesSent.incrementAndGet();


	}*/

  private static HashMap<String,FTPClient> ftpConnection(int totalClients)
      throws SocketException, IOException {
    HashMap<String,FTPClient> ftpClientList = new HashMap<String,FTPClient>();
    for (int i = 0; i < totalClients; i++) {

      String server = "localhost";
      int port = 5021;
      String user = "ftp_plain";
      String pass = "123";
      FTPClient ftpClient = new FTPClient();
      ftpClient.connect(server, port);
      int replyCode = ftpClient.getReplyCode();
      if (!FTPReply.isPositiveCompletion(replyCode)) {
        System.out.println("Operation failed. Server reply code: "
            + replyCode);
        throw new RuntimeException(
            "Operation failed. Server reply code: " + replyCode);
      }
      boolean success = ftpClient.login(user, pass);
      System.out.println("creating client  " + (i+1));
      ftpClient.mkd("client"+(i+1));
      if (!success) {
        System.out.println("Could not login to the server");
        throw new RuntimeException("connection to server failed.");
      } else {
        System.out.println("LOGGED IN SERVER");
        ftpClientList.put("client"+i,ftpClient);
      }

    }

    return ftpClientList;
  }

}
