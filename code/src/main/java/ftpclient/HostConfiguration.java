package ftpclient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class HostConfiguration {
  
  static String versalexHostDir = "/Users/rohit/software/vagrant/test/Har18/hosts";
  static String versalexDir = "/Users/rohit/software/vagrant/test/Har18";
  
  static String refernceHost = "LC.xml";
  static String tobeModfiedHost = "LC1.xml";

  
  public static void main(String[] args) throws Exception {
    
    
    int clientStartCounter = 1462;
    System.out.println(clientStartCounter);
//    replaceClientCounter(clientStartCounter);
    updateSchedulerSchedule("LC1", clientStartCounter);
    updateTriggerSchedule("LC1", clientStartCounter);
  }
  
  private static void replaceClientCounter(int clientStartCounter) throws Exception {
    String modFileData = getFileData(new File(versalexHostDir+"/" + tobeModfiedHost));
    for(int i=1;i<=clientStartCounter;i++) {
      modFileData = modFileData.replace("/ftp_plain/client"+i + "/*", "/ftp_plain/client"+(i+clientStartCounter) + "/*");
    }
    FileOutputStream outStream = new FileOutputStream(new File(versalexHostDir+"/" + tobeModfiedHost));
    outStream.write(modFileData.getBytes());
    outStream.flush();
    outStream.close();
  }
  
  public static void updateSchedulerSchedule(String host, int scheduleCount) throws Exception {
    String replaceString = "</Autostartup>";
    String pathTemplate = host+"\\scheduler\\schedule";
    String scheduleTemplate = "<Item path=\""+host+"\\scheduler\\schedule\">"
        + "<Itemperiod>Weekly</Itemperiod>   "
        + " <Onlyiffiletosend>True</Onlyiffiletosend>"
        + "    <Period>Weekly</Period>    "
        + "<Calendar days=\"Su-Sa\">"
        + "      <Time recurring=\"00:00\" start=\"00:00\" until=\"24:00\"/>   "
        + " </Calendar> "
        + " </Item>";
    StringBuffer bufferData = new StringBuffer();
    bufferData.append(replaceString);
    for(int i=1;i<scheduleCount;i++) {
      String item = scheduleTemplate.replace(pathTemplate, pathTemplate+(scheduleCount+i));
      bufferData.append(item+"\n");
    }
    File scheduleFile = new File(versalexDir+"/conf/Schedule.xml");
    String fileData = getFileData(scheduleFile);
    String data = fileData.replace(replaceString, bufferData);
    FileOutputStream stream = new FileOutputStream(scheduleFile);
    stream.write(data.getBytes());
    stream.close();
  }
  
  public static void updateTriggerSchedule(String host, int scheduleCount) throws Exception {
    String replaceString = "</Autostartup>";
    String pathTemplate = host+"\\trigger\\action";
    String scheduleTemplate = "<Item path=\""+host+"\\trigger\\action\">"
        + "<Itemtrigger>NewFileCreated</Itemtrigger>"
        + "<Onlyiffiletosend>False</Onlyiffiletosend>"
        + "</Item>";
    StringBuffer bufferData = new StringBuffer();
    bufferData.append(replaceString);
    for(int i=1;i<scheduleCount;i++) {
      String item = scheduleTemplate.replace(pathTemplate, pathTemplate+(scheduleCount+i));
      bufferData.append(item+"\n");
    }
    File scheduleFile = new File(versalexDir+"/conf/Schedule.xml");
    String fileData = getFileData(scheduleFile);
    String data = fileData.replace(replaceString, bufferData);
    FileOutputStream stream = new FileOutputStream(scheduleFile);
    stream.write(data.getBytes());
    stream.close();
  }
  
  private static int getMaxClientCount() throws Exception {
    String fileData = getFileData(new File(versalexHostDir+"/" + refernceHost));
    int lastIndexOfClientDir = fileData.lastIndexOf("/ftp_plain/client");
    System.out.println(lastIndexOfClientDir);
    System.out.println(fileData.length());
    String clientCount = fileData.substring(lastIndexOfClientDir+10,lastIndexOfClientDir+25);
    return Integer.parseInt(clientCount);
  }

  private static String getFileData(File refHostFile) throws FileNotFoundException, IOException {
    FileInputStream stream = new FileInputStream(refHostFile);
    byte[] fileBytes = new byte[new Long(refHostFile.length()).intValue()];
    stream.read(fileBytes);
    String fileData = new String(fileBytes);
    stream.close();
    return fileData;
  }

}
