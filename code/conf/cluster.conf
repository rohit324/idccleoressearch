session.force.timeout.in.seconds=30
# TEMP- remove this once topology based decision is made in code.
clusteredPersistorSetup=true
# IMPORTANT - DO NOT Change this once set.
noOfTopics=5
akka {
    loggers = ["akka.event.slf4j.Slf4jLogger"]
    logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
    loglevel = INFO
    stdout-loglevel = INFO
    event-handlers = ["akka.event.Logging$DefaultLogger"]
    log-dead-letters = 0
    log-dead-letters-during-shutdown = off

    actor {
      provider = "akka.cluster.ClusterActorRefProvider"


      serializers {
        kafka-event = "akka.persistence.kafka.journal.KafkaEventSerializer"
        kafka-snapshot = "akka.persistence.kafka.snapshot.KafkaSnapshotSerializer"
      }

      serialization-bindings {
        "akka.persistence.kafka.Event" = kafka-event
        "akka.persistence.kafka.snapshot.KafkaSnapshot" = kafka-snapshot
      }

    }

    remote {
      enabled-transports = ["akka.remote.netty.tcp"]
      log-remote-lifecycle-events = off
      netty.tcp {
        hostname = "<localip>"
        port = 2551 #${PORT}
      }
    }

    cluster {
      seed-nodes = [
        "akka.tcp://event-processing-cluster@<localip>:2551",
        "akka.tcp://event-processing-cluster@<otherip>:2551"
      ]
      
      # IMPORTANT: Detect split brain
      auto-down = on
      auto-down-unreachable-after = 60s

      min-nr-of-members = 1

    }

    persistence.journal.plugin = "kafka-journal"



  }




  kafka-journal {

    # FQCN of the Kafka journal plugin
    class = "akka.persistence.kafka.journal.KafkaJournal"

    # Dispatcher for the plugin actor
    plugin-dispatcher = "kafka-journal.default-dispatcher"

    # Number of concurrent writers (should be <= number of available threads in
    # dispatcher).
    write-concurrency = 2

    # The partition to use when publishing to and consuming from journal topics.
    partition = 0

    # Default dispatcher for plugin actor.
    default-dispatcher {
      type = Dispatcher
      executor = "fork-join-executor"
      fork-join-executor {
        parallelism-min = 2
        parallelism-max = 8
      }
    }

    consumer {
      # -------------------------------------------------------------------
      # Simple consumer configuration (used for message replay and reading
      # metadata).
      #
      # See http://kafka.apache.org/documentation.html#consumerconfigs
      # See http://kafka.apache.org/documentation.html#simpleconsumerapi
      # -------------------------------------------------------------------

      socket.timeout.ms = 30000

      socket.receive.buffer.bytes = 65536

      fetch.message.max.bytes = 1048576
    }

    producer {
      # -------------------------------------------------------------------
      # PersistentRepr producer (to journal topics) configuration.
      #
      # See http://kafka.apache.org/documentation.html#producerconfigs
      #
      # The metadata.broker.list property is set dynamically by the journal.
      # No need to set it here.
      # -------------------------------------------------------------------

      request.required.acks = 0

      # DO NOT CHANGE!
      producer.type = "sync"

      # DO NOT CHANGE!
      partitioner.class = "akka.persistence.kafka.StickyPartitioner"

      # DO NOT CHANGE!
      key.serializer.class = "kafka.serializer.StringEncoder"

      # Increase if hundreds of topics are created during initialization.
      message.send.max.retries = 15

      # Increase if hundreds of topics are created during initialization.
      retry.backoff.ms = 100

      # Add further Kafka producer settings here, if needed.
      # ...
    }

    event.producer {
      # -------------------------------------------------------------------
      # Event producer (to user-defined topics) configuration.
      #
      # See http://kafka.apache.org/documentation.html#producerconfigs
      # -------------------------------------------------------------------

      producer.type = "sync"

      request.required.acks = 0

      topic.mapper.class = "akka.persistence.kafka.DefaultEventTopicMapper"

      key.serializer.class = "kafka.serializer.StringEncoder"

      # Add further Kafka producer settings here, if needed.
      # ...
    }

    zookeeper {
      # -------------------------------------------------------------------
      # Zookeeper client configuration
      # -------------------------------------------------------------------

      connect = "<zkhost>:2181"

      session.timeout.ms = 6000

      connection.timeout.ms = 6000

      sync.time.ms = 2000
    }
}




